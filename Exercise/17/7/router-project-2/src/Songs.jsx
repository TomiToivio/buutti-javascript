import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import Song, { loader as songLoader } from "./Song";
import songList from "./songList";

export function loader({params}) {
    return params;
}

export default function Songs() {
    const params = useLoaderData();
    console.log("Songs");
    let songElements = [];
    for(let i = 0; i < songList.length; i++) {
        let songLink = "/" + i;
        songElements.push(<p><Link to={songLink}>{songList[i].title}</Link></p>);
    }
    return (
        <div className='Songs'>
            <nav>
                {songElements}
            </nav>
            <Outlet />
        </div>
    );
}
