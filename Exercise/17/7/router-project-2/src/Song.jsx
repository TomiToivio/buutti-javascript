import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState } from "react";
import songList from "./songList";

export function loader({params}) {
    return params;
}

export default function Book() {
    const params = useLoaderData();

    
    const [songs, setSongs] = useState(songList);

    return (
        <div className='Song'>
            <p>This is songs page {params.song}!</p>
            <p>Song title: {songs[Number(params.song)].title}</p>
            <p>Song lyrics: {songs[Number(params.song)].lyrics}</p> 
        </div>
    );
}
/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/