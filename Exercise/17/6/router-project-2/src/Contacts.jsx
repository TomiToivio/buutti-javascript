import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import Contact, { loader as contactLoader } from "./Contact";

export function loader({params}) {
    return params;
}

export default function Contacts() {
    const params = useLoaderData();
    console.log("Contacts");
    /* const defaultContacts = [{ "id": 1, "name": "Tomi Toivio", "email": "tomi.toivio@ukuli.fi"},{ "id": 2, "name": "Tomi Toivio", "email": "tomi@sange.fi"}];
    const [contacts, setContacts] = useState(defaultContacts);
    console.log(contacts);
    console.log(typeof params.contact);
    const newContact = () => {
        let newContacts = contacts;
        setContacts(newContacts);
    }; */
    return (
        <div className='Page'>
            <nav>
                <Link to={"/contacts/0"}>Contact 0</Link>
                <Link to={"/contacts/1"}>Contact 1</Link>
            </nav>
            <Outlet />
        </div>
    );
}
/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/