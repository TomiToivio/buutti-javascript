import ReactDOM from "react-dom/client";
import { useLoaderData } from 'react-router-dom'
import { useState } from "react";
import PropTypes from 'prop-types';
   
export function loader({params}) {
    return params;
}

export default function List({name,elements,decoration}) {
    const [contacts, setContacts] = useState([])

    const newContact = () => {
        let newContacts = [];
        setContacts(newContacts);
    }
    const params = useLoaderData()
    return <div className='Page'>
        This is page {params.page}!
    </div>
}

List.propTypes = {
    name: PropTypes.string.isRequired,
    elements: PropTypes.arrayOf(PropTypes.string),
    decorator: PropTypes.oneOf(['bold', 'italic', 'none']),
  };

/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/