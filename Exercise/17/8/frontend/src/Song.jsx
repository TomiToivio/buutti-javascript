import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState, useEffect } from "react";
//import songList from "./songList";

export function loader({params}) {
    return params;
}

export default function Book() {
    const params = useLoaderData();

    let emptySong = {
        "title": "",
        "lyrics": "",
    };
    //const [songs, setSongs] = useState(songList);
    const [songData, setSongData] = useState(emptySong);
    /*const [songID, setSongID] = useState(0);
    setSongID(params.song);*/

    /*
    useEffect(() => { 
        let fetchSong = fetch("http://localhost:3000/song/" + params.song);
        fetchSong.then(res =>
            res.json()).then(songObject => {
            setSongData(songObject);
        });
      }, []); */

    useEffect(() => { 
        /*fetch("http://localhost:3000/song/" + params.song).then(response => response.json()).then(json => {
            console.log(json);
            setSongData(json);
        }) */
        fetchSong();
    }, [params.song]);

    function fetchSong() {
        fetch("/song/" + params.song).then(response => response.json()).then(json => {
            console.log(json);
            setSongData(json);
           /* refreshPage();*/
        });
    }

    
    function refreshPage() {
        window.location.reload();
    }
  
    return (
        <div className='Song'>
            <p>Song title: {songData.title}</p>
            <p>Song lyrics: {songData.lyrics}</p> 
        </div>
    );
}
/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/