import postgres from 'postgres'
import config from 'config'
const pgpass = config.get(PGPASS);
const pguser = config.get(PGUSER);
const sql = postgres('postgres://username:password@host:port/database', {
  host                 : "localhost",            // Postgres ip address[s] or domain name[s]
  port                 : 5432,          // Postgres server port[s]
  database             : "react",            // Name of database to connect to
  username             : pguser,            // Username of database user
  password             : pgpass,            // Password of database user
})