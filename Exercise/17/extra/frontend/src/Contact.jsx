import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState } from "react";

export function loader({params}) {
    return params;
}

export default function Contact() {
    const params = useLoaderData();
    const defaultContacts = [{ "id": 1, "name": "Tomi Toivio", "email": "tomi.toivio@ukuli.fi"},{ "id": 2, "name": "Tomi Toivio", "email": "tomi@sange.fi"}];
    const [contacts, setContacts] = useState(defaultContacts);
    console.log(contacts);
    console.log(typeof params.contact);
    const newContact = () => {
        let newContacts = contacts;
        setContacts(newContacts);
    };
    return (
        <div className='Contact'>
            <p>This is contacts page {params.contact}!</p>
            <p>Contact name: {contacts[Number(params.contact)].name}</p>
            <p>Contact email: {contacts[Number(params.contact)].email}</p> 
        </div>
    );
}
/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/