import ReactDOM from "react-dom/client";
import { useLoaderData } from 'react-router-dom'

export function loader({params}) {
    return params;
}

export default function Admin() {
    const params = useLoaderData()
    return <div className='Page'>
        This is Admin!
    </div>
}
/*
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
*/
/* existing imports */
/*
import Root from "./routes/root";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
*/