import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState, useEffect } from "react";
import { Link, Outlet } from "react-router-dom";
import Song, { loader as songLoader } from "./Song";
import songList from "./songList";

export function loader({params}) {
    return params;
}

export default function Songs() {
    const params = useLoaderData();
    const emptySong = {
        "title": "Placeholder",
    };
    const [songsData, setSongsData] = useState([emptySong]);

    useEffect(() => { 
        fetch("http://localhost:3000/api/songs/").then(response => response.json()).then(json => {
            console.log(json);
            setSongsData(json);
        })
    }, []);

   /* console.log("Songs"); */
    let songElements = [];
    for(let i = 0; i < songsData.length; i++) {
        let songLink = "/" + i;
        songElements.push(<li><Link key={i} to={songLink}>{songsData[i].title}</Link></li>);
    }
    return (
        <div className='Songs'>
            <nav><ul>
                {songElements}
            </ul></nav>
            <Outlet />
        </div>
    );
}
