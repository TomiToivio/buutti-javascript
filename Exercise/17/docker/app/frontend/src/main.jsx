import React from "react";
import ReactDOM from "react-dom/client";
//import App from "./App";
import "./index.css";
import Page, { loader as pageLoader } from "./Page";
import Admin, { loader as adminLoader } from "./Admin";
import Contacts, { loader as contactsLoader } from "./Contacts";
import Contact, { loader as contactLoader } from "./Contact";
import { useRouteError, Link, createBrowserRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "./ErrorPage";
import PropTypes from 'prop-types';

const router = createBrowserRouter([
    {
        path: '/',
        element: <div>Hello App!</div>,
        errorElement: <ErrorPage />
    },
/*    {
        path: "/:page",
        element: <Page />,
        loader: pageLoader
    }, */
    {
        path: "/contacts",
        element: <Contacts />,
        //loader: pageLoader
        children: [
            {
                path: "/contacts/:contact",
                element: <Contact />,
                loader: pageLoader
            },
        ]
    },  
    {
        path: "/admin",
        element: <Admin />,
        loader: pageLoader
    }
]);

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);