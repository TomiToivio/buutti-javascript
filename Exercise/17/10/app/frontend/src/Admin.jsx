import ReactDOM from "react-dom/client";
import { useLoaderData } from 'react-router-dom'

export function loader({params}) {
    return params;
}

export default function Admin() {
    const params = useLoaderData()
    return <div className='Page'>
        This is Admin!
    </div>
}