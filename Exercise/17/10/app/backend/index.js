import express from "express";
const app = express();
import url from "url";
import path from "path";
import cors from "cors";
import config from "config";
const dbConfig = config.get("Default.dbConfig");
const PGHOST = dbConfig.PGHOST;
const PGPORT = dbConfig.PGPORT;
const PGDATABASE = dbConfig.PGDATABASE;
const PGUSER = dbConfig.PGUSER;
const PGPASSWORD = dbConfig.PGPASSWORD;
import pkg from "pg";
const { Client } = pkg;
const client = new Client({
    user: PGUSER,
    host: PGHOST,
    database: PGDATABASE,
    password: PGPASSWORD,
    port: PGPORT,
});
client.connect();

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url));
const distDirectory = path.resolve(currentDirectory, "../frontend/dist/");
console.log("Starting");

async function getAllSongs() {
    let allSongs = await client.query("SELECT * FROM songs");
    return allSongs.rows;
}

async function getSong(songID) {
    let singleSong = await client.query("SELECT * FROM songs WHERE id=" + songID);
    return singleSong.rows;
}

app.use(cors());

app.get("/api/songs/", async function (req, res) {
    let responseSongs = [];
    let allSongs = await getAllSongs();
    for(let i = 0; i < allSongs.length; i++) {
        let responseSong = {
            "id": allSongs[i].id,
            "title": allSongs[i].title
        };
        responseSongs.push(responseSong);
    }
    res.json(responseSongs);
});

app.get("/api/song/:id", async function (req, res) {
    let songID = req.params.id;
    let singleSong = await getSong(songID);
    let responseSong = {
        "id": singleSong[0].id,
        "title": singleSong[0].title,
        "lyrics": singleSong[0].lyrics,
    };
    res.json(responseSong);
});
app.use("/", express.static(distDirectory));
app.get("*", (req,res) => {
    res.sendFile("index.html", { root: distDirectory });
});
app.listen(3000);
console.log("I am listening on 3000");

app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader("Access-Control-Allow-Methods","GET,POST,PUT,PATCH,DELETE");
    res.setHeader("Access-Control-Allow-Methods","Content-Type","Authorization");
    next(); 
});