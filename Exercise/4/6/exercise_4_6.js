function calculator(operator, num1, num2) {
    if(operator === "+" || operator === "-" || operator === "*" || operator === "/") {
        let calculatorString = String(Number(num1) + String(operator) + Number(num2));
        return eval(calculatorString);
    } else if (operator !== "+" || operator !== "-" || operator !== "*" || operator !== "/") {
        return "Unknown operator";
    }
}

console.log(calculator("-",2,3));
console.log(calculator("+",-2,3));
console.log(calculator("/",2,3));
console.log(calculator("*",-2,3));
