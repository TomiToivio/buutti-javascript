class terranCitizenlUserName {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userNamePass = this.userNameAndPass(firstName,lastName);
    }
    userNameAndPass(firstName,lastName) {
        //let currentDate = new Date();
        //let now = currentDate.getFullYear();
        const yearStr = new Date().getFullYear().toString().substring()
        firstName = firstName.split(""); // Tätä ei tarvitse tehdä, indeksoiti toimii myös stringeille
        lastName = lastName.split("");
        let userName = "B" + String(yearStr)[2] + String(yearStr)[3] + firstName[0] + firstName[1] + lastName[0] + lastName[1];
        userName = userName.toLowerCase();
        let randomNumber = Math.floor((Math.random() * 10) + 1);
        let randomAlpha = String.fromCharCode(Math.floor((Math.random() * 100) + 128 ));
        let userPass = String(randomNumber) + firstName[0].toLowerCase() + lastName[lastName.length - 1].toUpperCase() + randomAlpha + String(yearStr)[2] + String(yearStr)[3]; 
        return [userName,userPass];
    };
}
let tomi = new terranCitizenlUserName("Tomi","Toivio");
console.log(tomi);