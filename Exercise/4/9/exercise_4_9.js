function getVowelCount(inputString) {
    inputString = inputString.toLowerCase().split("");
    let vowelCount = 0;
    
    inputString.forEach(function(item) {
        let vowelList = String("aeiouy");
        if(vowelList.includes(item)) {
            vowelCount++;
        }    
    });
    return vowelCount;
    
}
console.log(getVowelCount("abracadabra"));
