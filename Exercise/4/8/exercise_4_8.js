let temps = [1, 5, 9, 3];

function aboveAverageTemp(temps) {
    const average = (temps.reduce((a, b) => a + b, 0)) / temps.length;
    let aboveAverages = [];
    aboveAverages = temps.filter(temp => temp > average);
    return aboveAverages;
}
console.log(aboveAverageTemp(temps));