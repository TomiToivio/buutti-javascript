function checkExam(realAnswers, studentAnswers) {
    let examScore = 0;
    for(let i = 0; i < studentAnswers.length; i++) {
        if(studentAnswers[i] === "") {
            examScore = examScore + 0;
        } else if(studentAnswers[i] !== realAnswers[i]) {
            examScore = examScore - 1;
        } else {
            examScore = examScore + 4;
        }
    }
    if(examScore < 0) {
        examScore = 0;
    }
    return examScore;
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); 
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""])); 
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]));  
console.log(checkExam(["b", "c", "b", "a"], ["",  "a", "a", "c"])); 