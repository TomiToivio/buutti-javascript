likes([]); // "no one likes this"
likes(["John"]) // "John likes this"
likes(["Mary", "Alex"]) // "Mary and Alex like this"
likes(["John", "James", "Linda"]) // "John, James and Linda like this"
likes(["Alex", "Linda", "Mark", "Max"]) // must be "Alex, Linda and 2 others 
function likes(likers) {
    if(likers.length === 0) {
        console.log("no one likes this");
    } else if (likers.length === 1) {
        console.log(likers[0] + " likes this");
    } else if (likers.length === 2) {
        console.log(likers[0] + " and " + likers[1] + " like this.");
    } else if (likers.length === 3) {
        console.log(likers[0] + ", " + likers[1] + " and " + likers[2] + " like this.");
    } else if (likers.length > 3) {
        console.log(likers[0] + ", " + likers[1] + " and " + (likers.length - 2) + " others like this.");
    } 
} 