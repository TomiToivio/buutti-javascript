import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
let todoist = [];

function App() {
    const [newInput, setNewInput] = useState("");
    const [todo, setToDo] = useState([]);

    function ToDoListElement(props) {
        const ToDoList = todo.map((todoitem,i) => {
            console.log(todoitem);
            return(<p key={todoitem}>{todoitem}</p>);
        });

        console.log(ToDoList);
    
        return (
            <div><p>ToDoList:</p>
                {ToDoList}
            </div>
        );
    }

    const onInputChange = (event) => {
    //event.preventDefault();
        console.log(event);
        setNewInput(event.target.value); 
    };

    const handleInputClick = (event) => {
        console.log(event);
        todoist.push(newInput);
        setToDo(todoist);
        setNewInput("");
    };


    return (
        <div className="App">
            <div className='InputStuff'>
                <legend>Input:</legend>
                <input id="inputMe" type="text" onChange={onInputChange} value={newInput} />
                <button onClick={handleInputClick}>Submit</button>
            </div>
            <div><ToDoListElement /></div>
        </div>
    );
}

export default App;
