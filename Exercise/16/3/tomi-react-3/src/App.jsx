import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
function App() {
    const [bingo, setBingo] = useState([]);
    const BingoButton = () => {
        function bingoButton() {
            let bingoNumber = Math.floor((Math.random() * 50) + 1);
            setBingo([...bingo, bingoNumber]);
        }
        return (
            <button onClick={bingoButton}>Get Bingo</button>
        );
    };
    return (
        <div className="App">
        {bingo.map(bingoNumber => {
            return <div className="bingoNumber" key={bingoNumber}>{bingoNumber}</div>;
        })}
            <BingoButton></BingoButton>
        </div>
    );
}
export default App;
