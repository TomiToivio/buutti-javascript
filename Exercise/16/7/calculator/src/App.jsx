import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [result, setResult] = useState("")
  const [calculation, setCalculation] = useState("")

  const one = () => {
    let newResult = String(result) + String("1");
    setResult(newResult);
  }

  const two = () => {
    let newResult = String(result) + String("2");
    setResult(newResult);
  }

  const three = () => {
    let newResult = String(result) + String("3");
    setResult(newResult);
  }

  const four = () => {
    let newResult = String(result) + String("4");
    setResult(newResult);
  }

  const five = () => {
    let newResult = String(result) + String("5");
    setResult(newResult);
  }

  const six = () => {
    let newResult = String(result) + String("6");
    setResult(newResult);
  }

  const seven = () => {
    let newResult = String(result) + String("7");
    setResult(newResult);
  }

  const eight = () => {
    let newResult = String(result) + String("8");
    setResult(newResult);
  }

  const nine = () => {
    let newResult = String(result) + String("9");
    setResult(newResult);
  }

  const zero = () => {
    let newResult = String(result) + String("0");
    setResult(newResult);
  }

  const minus = () => {
    let newResult = String(result) + String("-");
    setResult(newResult);
  }
  const add = () => {
    let newResult = String(result) + String("+");
    setResult(newResult);
  }
  const divide = () => {
    let newResult = String(result) + String("/");
    setResult(newResult);
  }
  const multiply = () => {
    let newResult = String(result) + String("*");
    setResult(newResult);
  }

  const equals = () => {
    let newResult = eval(result);
    setResult(newResult);
  }

  const clear = () => {
    setResult("");
  }

  return (
    <div className="App">
    <div id="calculator">
    <div id="result">{result}</div>
    <p>
      <button onClick={one}>1</button>
      <button onClick={two}>2</button>
      <button onClick={three}>3</button>
    </p>
    <p>
      <button onClick={four}>4</button>
      <button onClick={five}>5</button>
      <button onClick={six}>6</button>
    </p>
    <p>
      <button onClick={seven}>7</button>
      <button onClick={eight}>8</button>
      <button onClick={nine}>9</button>
    </p>
    <p>
    <button onClick={zero}>0</button>
    </p>
    <p>
    <button onClick={add}>+</button>
    <button onClick={divide}>/</button>
    <button onClick={multiply}>*</button>
    <button onClick={minus}>-</button>
    </p>
    <p>
      <button onClick={equals}>=</button>
      <button onClick={clear}>C</button>
    </p>
    </div>
    </div>
  )
}

export default App
