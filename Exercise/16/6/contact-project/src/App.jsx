import React, { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import TextField from "@mui/material/TextField";
import './App.css'
import FormControlLabel, { formControlLabelClasses } from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
 
function App() {
  const [mode,setMode] = useState("read")
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState("")
  const [address, setAddress] = useState("")
  const [website, setWebsite] = useState("")
  const [info, setInfo] = useState("")
  const [newName, setNewName] = useState("")
  const [newEmail, setNewEmail] = useState("")
  const [newPhone, setNewPhone] = useState("")
  const [newAddress, setNewAddress] = useState("")
  const [newWebsite, setNewWebsite] = useState("")
  const [newInfo, setNewInfo] = useState("")
  const [id, setID] = useState(0)
  const oldContact = {"name": "Tomi Toivio",
                    "email": "tomi@sange.fi",
                    "phone": "0500000000",
                    "address":  "Espoo",
                    "website": "http://sange.fi",
                    "info": "I am an example."
                  };
  const [contacts, setContacts] = useState([oldContact]);
  const [search, setSearch] = useState("")
  const [searchText, setSearchText] = useState("")

  const renderSingleContact = (event) => {
    console.log(event.target.id);
    setMode("read");
    setID(event.target.id)
    setName(contacts[event.target.id].name)
    setEmail(contacts[event.target.id].email)
    setPhone(contacts[event.target.id].phone)
    setAddress(contacts[event.target.id].address)
    setWebsite(contacts[event.target.id].website)
    setInfo(contacts[event.target.id].info)  
  }
/*
  const SingleContact = (contactID) => {
    let searchItem = contacts[contactID];
    return(
        <div className="contactListItem">
        {console.log(searchItem)}
        <h3>{searchItem.name}</h3>
        <p>Email: {searchItem.email}</p>
        <p>Phone: {searchItem.phone}</p>
        <p>Address: {searchItem.address}</p>
        <p>Website: {searchItem.website}</p>
        <p>Info: {searchItem.info}</p> 
        </div> 
        )
  }  
*/
  const SearchResults = () => {
    console.log(search);
    let searchList = contacts;
    console.log(searchList);
    if(search !== "") {
      searchList = contacts.filter(item => item.name.toLowerCase().includes(search.toLowerCase()));
    }
    const itemRows = [];
    for(let index = 0; index < searchList.length; index++) {
      let searchItem = searchList[index];
      const row = (
      <button id={index} key={index} onClick={renderSingleContact}>{searchItem.name}</button>
      );
      itemRows.push(row);
    }
    return(
      <>
      {itemRows}
      </>
      );
    /*
    for(let index = 0; index < searchList.length; index++) {
      let searchItem = searchList[index];
      console.log(searchItem);
      console.log(index);
      return(
        <button id={index} key={index} onClick={renderSingleContact}>{searchItem.name}</button>
        )
      }
      */
    }

  useEffect(() => {
      initializeSearch();
  },[]);

  const initializeSearch = async (event) => {
    /* event.preventDefault(); */
    console.log(searchText);
    setSearch(searchText);
  };


const SingleContact = () => {
  if(mode === "read" && name !== "") {
  return(
  <div id="singleContactDiv">
  <h3>Name: {name}</h3>
  <p>Email: {email}</p>
  <p>Phone: {phone}</p>
  <p>Address: {address}</p>
  <p>Website: {website}</p>
  <p>Info: {info}</p>
  <button id="editButton" onClick={openEditEditor}>Edit</button>
  <button id="deleteButton" onClick={deleteContact}>Delete</button>
  </div>
  )
  }
  if(mode === "edit" && name !== "") {
    return(
    <div id="singleContactDiv">
    <form>
    <input type="text" id="nameInput" value={name} onChange={e => {e.preventDefault(); setName(e.target.value);}}></input>
    <input type="text" id="emailInput" value={email} onChange={e => {e.preventDefault(); setEmail(e.target.value);}}></input>
    <input type="text" id="phoneInput" value={phone} onChange={e => {e.preventDefault(); setPhone(e.target.value);}}></input>
    <input type="text" id="addressInput" value={address} onChange={e => {e.preventDefault(); setAddress(e.target.value);}}></input>
    <input type="text" id="websiteInput" value={website} onChange={e => {e.preventDefault(); setWebsite(e.target.value);}}></input>
    <input type="text" id="infoInput" value={info} onChange={e => {e.preventDefault(); setInfo(e.target.value);}}></input>
    <button id="saveButton" onClick={saveContact}>Save</button>
    </form>
    </div>
    )
    }  
    if(mode === "add") {
      console.log("Add contact");
      return(
      <div id="singleContactDiv">
      <form>
      <input type="text" id="nameInput" value={newName} onChange={e => {e.preventDefault(); setNewName(e.target.value);}}></input>
      <label for="nameInput">Name</label><br/>
      <input type="text" id="emailInput" value={newEmail} onChange={e => {e.preventDefault(); setNewEmail(e.target.value);}}></input>
      <label for="emailInput">Email</label><br/>
      <input type="text" id="phoneInput" value={newPhone} onChange={e => {e.preventDefault(); setNewPhone(e.target.value);}}></input>
      <label for="phoneInput">Phone</label><br/>
      <input type="text" id="addressInput" value={newAddress} onChange={e => {e.preventDefault(); setNewAddress(e.target.value);}}></input>
      <label for="addressInput">Address</label><br/>
      <input type="text" id="websiteInput" value={newWebsite} onChange={e => {e.preventDefault(); setNewWebsite(e.target.value);}}></input>
      <label for="websiteInput">Website</label><br/>
      <input type="text" id="infoInput" value={newInfo} onChange={e => {e.preventDefault(); setNewInfo(e.target.value);}}></input>
      <label for="infoInput">Info</label><br/>
      <button id="saveButton" onClick={addNewContact}>Add</button>
      </form>
      </div>
      )
      }  
}  
const addNewContact = (event) => {
  event.preventDefault();
  const newContact = {"name": newName,
  "email": newEmail,
  "phone": newPhone,
  "address":  newAddress,
  "website": newWebsite,
  "info": newInfo
  };
  let oldContacts = [...contacts];
  oldContacts.push(newContact);
  setContacts(oldContacts);
  setMode("read")
  setNewEmail("")
  setNewPhone("")
  setNewAddress("")
  setNewWebsite("")
  setNewInfo("")
}
const addContact = (event) => {
  console.log("Add contact");
  event.preventDefault();
  setMode("add");
}
const saveContact = (event) => {
  event.preventDefault();
  const editedContact = {"name": name,
  "email": email,
  "phone": phone,
  "address":  address,
  "website": website,
  "info": info
  };
  let oldContacts = [...contacts];
  oldContacts[id] = editedContact;
  setContacts(oldContacts);
  setMode("read")
}
const openEditEditor = (event) => {
  console.log("Open editor");
  event.preventDefault();
  setMode("edit")
}

const openEditor = (event) => {
  console.log("Open editor");
  event.preventDefault();
  setMode("add")
}
const deleteContact = (event) => {
  event.preventDefault();
  console.log("Deleting" + id);
  console.log("To delete: ", contacts[id]);
  let newContacts = contacts.filter((contact) => contact.name !== name);
  //let newContacts = contacts.filter((contact) => contact.name !== name);
  console.log(newContacts);
  setContacts(newContacts);
  console.log(contacts);
  setMode("empty")
}
/*
  const SearchForm = () => {
    return(
      <div id="searchDiv">
      <form>
      <input type="text" id="searchForm" value={searchText} onChange={e => {e.preventDefault(); setSearchText(e.target.value);}}></input>
      <button onClick={initializeSearch}>Search</button>
      </form>
      </div>
    )
  }
  */
 /*
  function SearchForm() {   
    return (
      <React.Fragment>
      <div id="searchDiv">
      <TextField
        key="searchtextfield"
        label="Search"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
        margin="normal"
      />
      <button onClick={initializeSearch}>Search</button>
      </div>
      </React.Fragment>
    );
  }
  */
  return (
    <div className="App">
      <div className="left">
      <React.Fragment>
      <div id="searchDiv">
      <TextField
        key="searchtextfield"
        label="Search"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
        margin="normal"
      />
      <button onClick={initializeSearch}>Search</button>
      </div>
      </React.Fragment>
      <SearchResults></SearchResults>
      <button id="addButton" onClick={openEditor}>Add</button>
      </div>
      <div className="right">
      <SingleContact></SingleContact>
      </div>
    </div>
  )
}

export default App
