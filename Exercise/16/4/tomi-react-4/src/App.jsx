import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

function App() {
    const [feedbacks, setFeedbacks] = useState([]);
    const [content, setContent] = useState("");
    const [email, setEmail] = useState("");
    const [feedbackType, setFeedbackType] = useState("");

    const FeedBackForm = () => {
        function submitForm(event) {
            event.preventDefault();
            console.log(event);
            let newFeedBack = {
                "text": content,
                "type": feedbackType,
                "email": email,
            };
            setFeedbacks([...feedbacks, newFeedBack]);
            setContent("");
            setEmail("");
            setFeedbackType("");
        }
        function resetForm(event) {
            event.preventDefault();
            console.log(event);
            setContent("");
            setEmail("");
            setFeedbackType("");
        }
        return(
            <form id="feedbackform">
                <input checked={feedbackType === "question"} type="radio" id="question" name="feedbackType" value="question" onChange={e => setFeedbackType(e.target.value)}></input>
                <label htmlFor="question">Question</label><br/>
                <input checked={feedbackType === "suggestion"} type="radio" id="suggestion" name="feedbackType" value="suggestion" onChange={e => setFeedbackType(e.target.value)}></input>
                <label htmlFor="suggestion">Suggestion</label><br/>
                <input checked={feedbackType === "feedback"} type="radio" id="feedback" name="feedbackType" value="feedback" onChange={e => setFeedbackType(e.target.value)}></input>
                <label htmlFor="feedback">Feedback</label><br/>
                <input type="text" id="content" value={content}
                    onChange={e => {e.preventDefault(); setContent(e.target.value);}}></input>
                <label htmlFor="content">Your feedback</label><br/>
                <input type="email" id="email" value={email}
                    onChange={e => { e.preventDefault(); setEmail(e.target.value);}}></input>
                <label htmlFor="email">Your email</label><br/>
                <button disabled={!feedbackType || !content || !email} onClick={submitForm}>Submit</button>
                <button onClick={resetForm}>Reset</button>
            </form>
        );
    };

    return (
        <div className="App">
            {feedbacks.map((feedback, index) => {
                return <div key={index} className="feedback"><h3>{feedback.type}</h3>
                    <p>{feedback.text}</p><p><i>{feedback.email}</i></p></div>;
            })}
            <FeedBackForm></FeedBackForm>
        </div>
    );
}

export default App;