import {useEffect, useState} from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

function App() {
    const [timer, setTimer] = useState(0);
    const [timerOn, setTimerOn] = useState(1);

    const TimerComponent = () => {

        useEffect(() => {
            let timeout = setTimeout(() => {
                if(timerOn) {
                    setTimer(timer+1); 
                } else {
                    setTimer(timer);
                }
            },
            1000);
            return () => clearTimeout(timeout);
        }, [timer]);

        const TimerFormatter = () => {
            let secondMinute = "";
            if((timer / 60) < 10) {
                secondMinute = String("0");
            } else {
                secondMinute = String("");
            }
            let secondZero = "";
            if((timer % 60) < 10) {
                secondZero = String("0");
            } else {
                secondZero = String("");
            }
            return(<div id="timer"><h1>{secondMinute + String(Math.floor(timer/60)) + ":" + secondZero + String(timer % 60)}</h1></div>);
        };

        return (
            <div id="timer"><TimerFormatter></TimerFormatter></div>
        // <div id="timer">{String(Math.floor(timer/60)) + ":" + String(timer % 60)}</div>
        );
    };

    const ZeroComponent = () => {
  
        function goToZero() {
            setTimer(0);
        }

        return (
            <button onClick={goToZero}>Zero Timer</button>
        );
    };

    const SecondComponent = () => {
  
        function addSecond() {
            setTimer(timer+1);
        }

        return (
            <button onClick={addSecond}>Add Second</button>
        );
    };

    const MinuteComponent = () => {
  
        function addMinute() {
            setTimer(timer+60);
        }

        return (
            <button onClick={addMinute}>Add Minute</button>
        );
    };

    const StopTimer = () => {
  
        function stopTimer() {
            setTimerOn(0);
        }

        return (
            <button onClick={stopTimer}>Stop Timer</button>
        );
    };

    const StartTimer = () => {
  
        function startTimer() {
            setTimerOn(1);
        }

        return (
            <button onClick={startTimer}>Start Timer</button>
        );
    };

    return (
        <div className="App">
            <TimerComponent></TimerComponent>
            <SecondComponent></SecondComponent>
            <MinuteComponent></MinuteComponent>
            <ZeroComponent></ZeroComponent>
            <StopTimer></StopTimer>
            <StartTimer></StartTimer>
        </div>
    );
}

export default App;
