import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [catSource, setCatSource] = useState("https://cataas.com/cat")
  const [catText, setCatText] = useState("")

  useEffect(() => {
    initialize();
  },[]);

  const initialize = async () => {
    if(catText !== "") {
      setCatSource("https://cataas.com/cat/says/" + catText);
    } else {
      setCatSource(`https://cataas.com/cat?${Date.now()}`);
    }
    console.log(catSource);
    setCatText("");
    //if(catText = "") {
    //  const response = await fetch("https://cataas.com/cat/says/" + catText);
    //} else {
    //  const response = await fetch("https://cataas.com/cat");
    //}
    //console.log(response);
    //setCat(response);
  };


  return (
    <div className="App">
      <img src={catSource}></img>
      <p>
      <input type="text" id="catText" value={catText} onChange={e => {e.preventDefault(); setCatText(e.target.value);}}></input>
      <button onClick={initialize}>Change Text</button>
      </p>
    </div>
  )
}

export default App
