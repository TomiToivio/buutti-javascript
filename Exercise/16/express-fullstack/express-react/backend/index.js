const express = require("express");
const cors = require("cors");
const app = express();
const PORT = 3000;

app.use(express.static("../frontend/dist"));

app.get("/quote", (req,res) => {
    res.send("Logic strictly qualifies that the needs of the many...");
});

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));

