import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { useEffect } from "react";

function App() {
    const [quote, setQuote] = useState("");

    const KurtCobain = () => {
        return(<p>Not in Foo Fighters</p>);
    };

    useEffect(() => {
        initialize();
    },[]);

    const initialize = async () => {
        const response = await fetch("http://127.0.0.1:3000/quote");
        const body = await response.text();
        setQuote(body);
    };

    return (
        <div className="App">
            <h1>Foo</h1><h3>Fighters</h3>
            <KurtCobain></KurtCobain>
            <p>{quote}</p>
        </div>
    );
}

export default App;
