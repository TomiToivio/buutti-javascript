/*
Create a forum page which has the following functionalities:
Some kind of a header (no special functionalities needed for this)
Two input fields:
One for user’s name
One for user’s post
A submit button
When the user clicks this, the name and the post will be posted to the page
Toggle button
Create a button that hides the form when clicked and shows the form when clicked again
Delete button for posts
Add a button to a forum post that removes it
Alert an error message if name or post are empty when submitting
*/
let nextToAdd = 1;
let formDisplay = "false";
function showForm() {
    let createPostForm = document.getElementById("createPostForm");
    let formShowButton = document.getElementById("formShowButton");
    if(formDisplay === "false") {
        createPostForm.setAttribute("style", "display:block;");
        formDisplay = "true";
        formShowButton.textContent = "Hide Our Suberb Editor";
    } else {
        createPostForm.setAttribute("style", "display:none;");
        formDisplay = "false";
        formShowButton.textContent = "Create A Superb Post";
    }
}
function postNow(event) {
    event.preventDefault();
    let postName = document.getElementById("postName");
    let postContent = document.getElementById("postContent");
    let postNameValue = postName.value;
    let postContentValue = postContent.value;
    if(postNameValue === "" || postContentValue === "") {
        alert("Fields cannot be empty!");      
    } else {
        let newPostDiv = document.createElement("div");
        let contentDiv = document.getElementById("contentDiv");
        contentDiv.appendChild(newPostDiv);
        newPostDiv.setAttribute("id",nextToAdd);
        newPostDiv.innerHTML = "<button onclick=\"removeOld(" + nextToAdd + ")\">Remove</button>";    
        let nameH = document.createElement("h3"); 
        newPostDiv.appendChild(nameH);
        nameH.textContent = postNameValue;
        let contentP = document.createElement("p");
        newPostDiv.appendChild(contentP);
        contentP.textContent = postContentValue;
        postName.value = "";
        postContent.value = "";          
    }
}
function removeOld(removeID) {
        console.log("removeOld()");
        let oldID = document.getElementById(removeID);
        oldID.remove();
}    
