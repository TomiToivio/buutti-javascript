import ReactDOM from "react-dom/client";
import { useLoaderData } from "react-router-dom";
import { useState, useEffect } from "react";

export function loader({params}) {
    return params;
}

function Song() {
    const params = useLoaderData();

    let emptySong = {
        "title": "",
        "lyrics": "",
    };
    const [songData, setSongData] = useState(emptySong);

    useEffect(() => { 
        fetchSong();
    }, [params.song]);

    function fetchSong() {
        fetch("http://localhost:3000/api/song/" + params.song).then(response => response.json()).then(json => {
            setSongData(json);
        });
    }  
    return (
        <div className='Song'>
            <h2>{songData.title}</h2>
            <pre>{songData.lyrics}</pre> 
        </div>
    );
}

export default Song;
