import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Admin, { loader as adminLoader } from "./Admin";
import Songs, { loader as songsLoader } from "./Songs";
import Song, { loader as songLoader } from "./Song";
import { useRouteError, Link, createBrowserRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "./ErrorPage";
import PropTypes from 'prop-types';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Songs />,
        loader: songsLoader,
        errorElement: ErrorPage,
        children: [
            {
                path: "/:song",
                element: <Song />,
                loader: songLoader,
            },
        ]
    },  
    {
        path: "/admin",
        element: <Admin />,
        loader: adminLoader
    }
]);

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);

export default router;