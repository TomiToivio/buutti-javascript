import React from "react";
import { vi, describe, it, expect } from "vitest";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { useLoaderData, useRouteError, MemoryRouter, Link, createBrowserRouter, RouterProvider } from "react-router-dom";
import Admin, { loader as adminLoader } from "../src/Admin";
import Songs, { loader as songsLoader } from "../src/Songs";
import Song, { loader as songLoader } from "../src/Song";
import router from "../src/main";

describe("Debugger", () => {
    test("check Songs components", () => {
        render(<Songs />, { wrapper: router });
        screen.debug();
    //const pageTitle = screen.getByRole('heading', { name: /mina/i });
    //expect(pageTitle).toBeInTheDocument();
    });
});
/*
describe('Todos list', () => {
    it('length is zero', () => {
        fetch.once(JSON.stringify({ id: 1, status: false, item: 'item 1' }));
        render(<App />)
        screen.debug()
        const items = screen.queryAllByRole('listitem')
        expect(items).toHaveLength(0)
    })
})
*/
/*
describe('User event', () => {
it('adds single item', async () => {
    fetch.once(JSON.stringify({ id: 1, status: false, item: 'item 1' }));
    render(<App/>);
    screen.debug();
    const user = userEvent.setup()
    const input = screen.getByRole('input',{"hidden":true})
    const button = screen.getByRole('button',{"hidden":true})
    const todoItemText = "Testi";
    fetch.once(JSON.stringify({ id: 2, status: false, item: todoItemText }));
    await user.type(input, todoItemText)
    await user.click(button)
    const items = screen.getAllByRole('listitem')
    expect(items).toHaveLength(1)
    expect(items[0]).toHaveTextContent(todoItemText)
    expect(items[0]).not.toHaveClass('checked')
})
})
*/