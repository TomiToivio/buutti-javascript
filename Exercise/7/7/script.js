document.getElementById("alcoholButton").addEventListener("click", function(event){
    event.preventDefault();
    let alcoholForm = document.getElementById("drinkForm"); 
    let drinkSizes = document.getElementById("drinkSizes").value; //ml
    let drinkAlcoholContent = document.getElementById("drinkAlcoholContent").value; // percent
    let drinkAmount = document.getElementById("drinkAmount").value; // glasses
    let weight = document.getElementById("weight").value; // kg 
    let timeSinceFirstDrink = document.getElementById("timeSinceFirstDrink").value; // hours
    let maleOrFemale = document.getElementById("maleOrFemale").value;
    let litres = (drinkSizes * drinkAmount) / 100;
    let grams = litres * 8 * drinkAlcoholContent;
    let burning = weight / 10;
    let gramsLeft = grams - (burning - timeSinceFirstDrink);
    let result = 0;
    const genderCoefficient = maleOrFemale === "male" ? 0.7 : 0.6
    result = gramsLeft / (weight * genderCoefficient)
    document.getElementById("yourAlcoholism").textContent = "Your alcoholism is at " + result + "%!;";
  });
  