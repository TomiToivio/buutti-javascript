import fetch from "node-fetch";
async function fetchData() {
    let totalResult = [];
    let url = "https://jsonplaceholder.typicode.com/todos/";
    fetch(url).then(response => response.json()).then(data => {
        let userUrl = "https://jsonplaceholder.typicode.com/users/";
        fetch(userUrl).then(response => response.json()).then(userData => {
            const result = mixData(data, userData);
            console.log(result);
        });
    });
}

async function mixData(data,userData) {
    let result = [];
    for (let index = 0; index < data.length; index++) {
        let userFind = userData.find((obj) => {
            return obj.id === data[index]["userId"];
        });        
        let user = {
            "name": userFind.name,
            "username": userFind.username,
            "email": userFind.email,
        };
        let newPost = {
            "id": data[index]["id"],
            "title": data[index]["title"],
            "completed": data[index]["completed"],
            "user": user,     
        };
        result.push(newPost);
    }
    return result;
}
fetchData();