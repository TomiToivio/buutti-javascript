    let hasDot = "false";
    let calculation = "";
    let result = "";
    let currentNumber = "";
    function updateCalculation() {
        let calculationDiv = document.getElementById("calculation");
        calculationDiv.textContent = calculation;
    }
    function updateResult() {
        let resultDiv = document.getElementById("result");
        resultDiv.textContent = result;
    }
    function one() {
        calculation = calculation + "1";
        updateCalculation();
    }
    function two() {
        calculation = calculation + "2";
        updateCalculation();
    }
    function three() {
        calculation = calculation + "3";
        updateCalculation();
    }
    function four() {
        calculation = calculation + "4";
        updateCalculation();
    }
    function five() {
        calculation = calculation + "5";
        updateCalculation();
    }
    function six() {
        calculation = calculation + "6";
        updateCalculation();
    }
    function seven() {
        calculation = calculation + "7";
        updateCalculation();
    }
    function eight() {
        calculation = calculation + "8";
        updateCalculation();
    }
    function nine() {
        calculation = calculation + "9";
        updateCalculation();
    }
    function zero() {
        calculation = calculation + "0";
        updateCalculation();
    }

    function dot() {
        let lastChar = calculation.charAt(calculation.length - 1);  
        console.log(lastChar);
        if((hasDot === "false") &&(lastChar !== ".") && (lastChar !== "*") && (lastChar !== "/") && (lastChar !== "-") && (lastChar !== "+")) {
            calculation = calculation + ".";
            hasDot = "true";
        } else {
            alert("Forbidden");
        }
        updateCalculation();
    }
    function add() {
        calculation = calculation + "+";
        hasDot = "false";
        updateCalculation();
    }
    function subtract() {
        calculation = calculation + "-";
        hasDot = "false";
        updateCalculation();
    }
    function divide() {
        calculation = calculation + "/";
        hasDot = "false";
        updateCalculation();
    }
    function multiply() {
        calculation = calculation + "*";
        hasDot = "false";
        updateCalculation();
    }
    function ac() {
        calculation = "";
        result = "";
        hasDot = "false";
        updateCalculation();
        updateResult();
    }
    function equals() {
        if(calculation.includes("+")) {
            calculation = calculation.split("+");
            result = calculation[0] + calculation[1];
        }
        if(calculation.includes("-")) {
            calculation = calculation.split("-");
            result = calculation[0] - calculation[1];
        }
        if(calculation.includes("*")) {
            calculation = calculation.split("*");
            result = calculation[0] * calculation[1];
        }
        if(calculation.includes("/")) {
            calculation = calculation.split("/");
            result = calculation[0] / calculation[1];
        }
        updateResult();
    }