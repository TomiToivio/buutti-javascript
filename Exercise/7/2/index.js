// Tämä on niin kamala ettei voi vaihtaa järkevämpään versioon.
function counterThing(counter) {
    setTimeout(() => {
        new Promise((resolve,_reject) => {
            if(counter > 0) {
                // Turn negative into positive
                console.log("..".repeat(-(counter - 3)) + counter);
                counter--;
                counterThing(counter);
            } else if (counter === 0) {
                console.log("GO!");
            }
            resolve("Promise resolved!");        
        })
    } , 1000);
};
counterThing(3);