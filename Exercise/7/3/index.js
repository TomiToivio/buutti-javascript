const getValue = function () {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ value: Math.random() });
        }, Math.random() * 1500);
    });
}

async function useAwait() {
    const valueOne = await getValue();
    console.log(valueOne);
    const valueTwo = await getValue();
    console.log(valueTwo);
}
function useThen() {
/* const valueThree = */ getValue().then((value) => {
    console.log(value);
});
/* const valueFour = */ getValue().then((value) => {
    console.log(value);
});
}
useAwait();
useThen();

/*
const someArrayOfPromises = someData.map(async (element) => {
    const resp = await axios.get(`someUrl/${element.id}`);
    return {...element, user: resp.data};
})

const data = await Promise.all(someArrayOfPromises);
*/
/*async function f() {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("done!"), 1000)
    });
    let result = await promise; // wait until the promise resolves
        console.log(result);
}
    
    
f();
*/
/*
async function foo() {
    await wait(1000);
    return "foobar";
}

foo().then(x => console.log(x));

async function wait(time) {
    setTimeout(() => {
        console.log("Foo");
        return "foo";
    }, time);
}
*/