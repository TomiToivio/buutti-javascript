import express from "express";
const userRouter = express.Router();
import argon2 from "argon2";
import "dotenv/config";
const PASSWORD = process.env.PASSWORD;
const USERNAME = process.env.ADMINUSER;
import jwt from "jsonwebtoken";
import sqlite3 from "sqlite3";

let db = new sqlite3.Database("./database/books.db", (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log("Connected to the books database.");
    createTable();
});

function createTable() {
    db.run("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username TEXT NOT NULL, userpass TEXT NOT NULL, isAdmin BOOLEAN NOT NULL)");
}

let sql = `SELECT * FROM users`;

userRouter.post("/register", (request, response) => {
    if (request.body.username && request.body.password) {
        argon2.hash(request.body.password).then(
            hashedPassword => {
                let newUser = {
                    "username": request.body.username,
                    "password": hashedPassword
                };
                const searchUser = users.find((user) => user.username == request.body.username);
                if(searchUser) {
                    response.status(400).send({status:"400"});
                } else {
                    users.push(newUser);
                    console.log(users);
                    console.log(newUser);
                    const payload = { "username": request.body.username, "isAdmin": false };
                    const secret = process.env.SECRET;
                    const options = { expiresIn: "1h"};
                    const token = jwt.sign(payload, secret, options);
                    console.log(token);
                    response.json({ "jwt": token });
                }
            });
    } else {
        response.status(400).send({error:"400"});
    }
});

userRouter.post("/login", (request, response) => {
    if (request.body.username && request.body.password) {
        const searchUser = users.find((user) => user.username == request.body.username);
        console.log(searchUser);
        argon2.verify(searchUser.password, request.body.password).then(argon2Match => {
            console.log(argon2Match);
            if(argon2Match) {
                console.log(searchUser.password);
                console.log(request.body.password);
                const payload = searchUser;
                const secret = process.env.SECRET;
                const options = { expiresIn: "1h"};
                const token = jwt.sign(payload, secret, options);
                console.log(token);
                response.json({ "jwt": token });
            } else {
                response.status(400).send({status:"400"});
            }
        });
    } else {
        response.status(400).send({error:"400"});
    }
});
export default userRouter;