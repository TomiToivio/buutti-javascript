import express from "express";
const server = express();
import "dotenv/config";

server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(3000, () => {
});

import userRouter from "./userRouter.js";
server.use("/user/v1/", userRouter);

import bookRouter from "./bookRouter.js";
server.use("/book/v1/", bookRouter);

export default server;
