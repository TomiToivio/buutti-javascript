import argon2 from "argon2";

const password = process.argv[2];
argon2.hash(password).then(result => console.log(result));