import fetch from "node-fetch";
import argon2 from "argon2";
import "dotenv/config";

const url = "http://localhost:3000/user/v1/login";

const PASSWORD = process.env.PASSWORD;
const USERNAME = process.env.ADMINUSER;

const user = {
    username: USERNAME,
    password: PASSWORD
};

console.log(user);

const options = {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + process.argv[2]
    } 
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));