import argon2 from "argon2";

const hash = "$argon2id$v=19$m=65536,t=3,p=4$AO5axA6j2PFDnpdQg9HZWA$YxjRZRbxk7YrXbWpojWNYsbbOVOvik3mPYnA5OUR6+o";
const password = process.argv[2];
argon2.verify(hash, password).then(passwordMatchesHash => console.log(passwordMatchesHash ? "Correct" : "Incorrect"));