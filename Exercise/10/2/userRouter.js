import express from 'express';
const userRouter = express.Router();
import argon2 from "argon2";

let users = [];

userRouter.post("/register", (request, response) => {
    if (request.body.username && request.body.password) {
        argon2.hash(request.body.password).then(
            hashedPassword => {
                let newUser = {
                    "username": request.body.username,
                    "password": hashedPassword
                };
                const searchUser = users.find((user) => user.username == request.body.username);
                if(searchUser) {
                    response.status(400).send({status:"400"});
                } else {
                    users.push(newUser);
                    console.log(users);
                    console.log(newUser);
                    response.status(201).send({status:"201"});
                }
            });
    } else {
        response.status(400).send({error:"400"});
    }
});

userRouter.post("/login", (request, response) => {
    if (request.body.username && request.body.password) {
        const searchUser = users.find((user) => user.username == request.body.username);
        console.log(searchUser);
        argon2.verify(searchUser.password, request.body.password).then(argon2Match => {
            console.log(argon2Match);
            if(argon2Match) {
                console.log(searchUser.password);
                console.log(request.body.password);
                response.status(200).send({status:"200"});
            } else {
                response.status(400).send({status:"400"});
            }
        });
    } else {
        response.status(400).send({error:"400"});
    }
});

export default userRouter;