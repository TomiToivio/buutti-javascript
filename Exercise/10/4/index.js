import express from "express";
const server = express();
import {fileURLToPath} from "url";
import path from "path";
import "dotenv/config";
const PASSWORD = process.env.PASSWORD;
const PORT = process.env.PORT;

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));
server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(PORT, () => {
    console.log("Listening to port " + PORT);
});

import studentRouter from "./studentRouter.js";
server.use("/student/", studentRouter);

import userRouter from "./userRouter.js";
server.use("/user/", userRouter);

import {loggerMiddleware, unknownEndpoint} from "./middlewares.js";

server.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/static/index.html"));
});

server.use(unknownEndpoint);