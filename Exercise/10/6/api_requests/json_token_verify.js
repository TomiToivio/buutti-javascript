import "dotenv/config";
import jwt from "jsonwebtoken";

console.log(process.env.SECRET);
console.log(process.env.JSON_TOKEN);

let decoded = jwt.verify(process.env.JSON_TOKEN, process.env.SECRET);
console.log(decoded);
