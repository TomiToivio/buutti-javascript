import fetch from "node-fetch";

const url = "http://localhost:3000/student";

const student = {
    id: 1,
    name: "Tomi Toivio",
    email: "tomi@sange.fi"
};

const options = {
    method: "POST",
    body: JSON.stringify(student),
    headers: {
        "Content-Type": "application/json"
    }
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));