import fetch from "node-fetch";

const url = "http://localhost:3000/user/v1/admin";

const user = {
    username: "TomiToivio",
    password: "salasana"
};

const options = {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
        "Content-Type": "application/json",
    } 
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));