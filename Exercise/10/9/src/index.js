//Add token based authentication to the Books API created in the previous assignment set. The API should have two endpoints /api/v1/users/login and /api/v1/users/register that handle the login and registration. Both should expect two body parameters, username and password.
//Login endpoint should check if the user exists. If the user does exist, it should compare the hashed password against the password user has sent. If the passwords match, it should send a response with a signed JSON Web Token.
//Register endpoint should check if the user exists. If the user does not exist, it should hash the received password and store the username and the hash in an in-memory storage. On success, it should send a response with a signed JSON Web Token.
//All book routes should be secured to require a bearer token to access them. On any error a response with appropriate status code and error message should be sent.
//Hint: Use separate routers for books and users.
import express from "express";
const server = express();
import "dotenv/config";

server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(3000, () => {
    // console.log('Listening to port 3000');
});

import userRouter from "./userRouter.js";
server.use("/user/v1/", userRouter);

import bookRouter from "./bookRouter.js";
server.use("/book/v1/", bookRouter);


