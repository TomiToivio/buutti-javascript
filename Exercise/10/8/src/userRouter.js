import express from "express";
const userRouter = express.Router();
import argon2 from "argon2";
import "dotenv/config";
const PASSWORD = process.env.PASSWORD;
const USERNAME = process.env.ADMINUSER;
import jwt from "jsonwebtoken";

let users = [];
/*
const authenticate = (req, res, next) => {
    const auth = req.get("Authorization");
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Invalid token");
    }
    const token = auth.substring(7);
    const secret = process.env.SECRET;
    try {
        const decodedToken = jwt.verify(token, secret);
        req.user = decodedToken;
        next();
    } catch (error) {
        return res.status(401).send("Invalid token");
    }
};
*/
userRouter.post("/register", (request, response) => {
    if (request.body.username && request.body.password) {
        argon2.hash(request.body.password).then(
            hashedPassword => {
                let newUser = {
                    "username": request.body.username,
                    "password": hashedPassword
                };
                const searchUser = users.find((user) => user.username == request.body.username);
                if(searchUser) {
                    response.status(400).send({status:"400"});
                } else {
                    users.push(newUser);
                    console.log(users);
                    console.log(newUser);
                    const payload = { username: request.body.username, "admin": true };
                    const secret = process.env.SECRET;
                    const options = { expiresIn: "1h"};
                    const token = jwt.sign(payload, secret, options);
                    console.log(token);
                    response.json({ "jwt": token });
                    response.status(201).send({status:"201"});
                }
            });
    } else {
        response.status(400).send({error:"400"});
    }
});

userRouter.post("/login", (request, response) => {
    if (request.body.username && request.body.password) {
        const searchUser = users.find((user) => user.username == request.body.username);
        console.log(searchUser);
        argon2.verify(searchUser.password, request.body.password).then(argon2Match => {
            console.log(argon2Match);
            if(argon2Match) {
                console.log(searchUser.password);
                console.log(request.body.password);
                const payload = { username: request.body.username, "admin": true };
                const secret = process.env.SECRET;
                const options = { expiresIn: "1h"};
                const token = jwt.sign(payload, secret, options);
                console.log(token);
                response.json({ "jwt": token });
                response.status(200).send({status:"200"});
            } else {
                response.status(400).send({status:"400"});
            }
        });
    } else {
        response.status(400).send({error:"400"});
    }
});

userRouter.post("/admin", (request, response) => {
    console.log("/admin");
    if (request.body.username && request.body.password) {
        console.log(request.body.password);
        console.log(USERNAME);
        console.log(PASSWORD);
        if(request.body.username == USERNAME) {
            argon2.verify(PASSWORD, request.body.password).then(argon2Match => {
                console.log(argon2Match);
                if(argon2Match) {
                    console.log(PASSWORD);
                    console.log(request.body.password);
                    console.log(process.env.SECRET);
                    const payload = { username: request.body.username, "admin": true };
                    const secret = process.env.SECRET;
                    const options = { expiresIn: "1h"};
                    const token = jwt.sign(payload, secret, options);
                    console.log(token);
                    response.json({ "jwt": token });
                    response.status(200).send({status:"200"});
                } else {
                    response.status(400).send({status:"400"});
                }
            });
        } else {
            response.status(400).send({error:"400"});
        }
    } else {
        response.status(400).send({error:"400"});
    }
});

export default userRouter;