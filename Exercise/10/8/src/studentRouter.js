import express from "express";
const studentRouter = express.Router();
import jwt from "jsonwebtoken";

let students = [];

const authenticate = (req, res, next) => {
    const auth = req.get("Authorization");
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Invalid token");
    }
    const token = auth.substring(7);
    const secret = process.env.SECRET;
    try {
        const decodedToken = jwt.verify(token, secret);
        req.user = decodedToken;
        next();
    } catch (error) {
        return res.status(401).send("Invalid token");
    }
};

studentRouter.get("/all", (_request, response) => {
    let responseBody = "<html><head></head><body><ul>";
    students.forEach((student) => {
        responseBody = responseBody + "<li>" + student.id + "</li>";
    });
    responseBody = responseBody + "</ul></body></html>";
    response.send(responseBody);
});

studentRouter.get("/:id", (request, response) => {
    const searchStudent = students.find((student) => student.id == request.params.id);
    if(searchStudent) {
        let responseStudent = {
            "id": searchStudent.id,
            "name": searchStudent.name,
            "email": searchStudent.email,
        };
        response.json(responseStudent);
    } else {
        response.status(404).send({error:"404"});
    }
});

studentRouter.post("/", authenticate, (request, response) => {
    if (request.body.id && request.body.name && request.body.email) {
        let newStudent = {
            "id": request.body.id,
            "name": request.body.name,
            "email": request.body.email
        };
        const searchStudent = students.find((student) => student.id == request.body.id);
        if(searchStudent) {
            // Mikä olikaan response status jos opiskelija on jo olemassa?
            response.status(201).send({status:"201"});
        } else {
            students.push(newStudent);
            response.status(200).send({status:"200"});
        }
    } else {
        response.status(400).send({error:"400"});
    }
});

studentRouter.put("/:id", authenticate, (request, response) => {
    if (request.body.name || request.body.email) {
        let studentIndex = students.findIndex((student => student.id == request.params.id));
        console.log(studentIndex);
        if(request.body.name) {
            students[studentIndex].name = request.body.name;
        }
        if(request.body.email) {
            students[studentIndex].email = request.body.email;
        }
        console.log(students);
        response.status(200).send({status:"200"});
    } else {
        response.status(400).send({error:"400"});
    }
});

studentRouter.delete("/:id", authenticate, (request, response) => {
    function isDeletable(student) { 
        return Number(student.id) !== Number(request.params.id); 
    } 
    function deleteStudent() { 
        students = students.filter(isDeletable); 
    } 
    deleteStudent();
    response.status(200).send({status:"200"});
});

export default studentRouter;