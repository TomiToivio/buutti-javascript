import fetch from "node-fetch";

const url = "http://localhost:3000/student/1";

const student = {
    //id: 1,
    name: "Tomi Toivio",
    email: "tomi@flossmanuals.net"
};

const options = {
    method: "PUT",
    body: JSON.stringify(student),
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + process.argv[2]
    } 
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));