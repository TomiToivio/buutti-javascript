import "dotenv/config";
import jwt from "jsonwebtoken";

console.log(process.env.secret);

const payload = { username: "TomiToivio", "role": "admin", "admin": true };
const secret = process.env.SECRET;
const options = { expiresIn: "1h"};

const token = jwt.sign(payload, secret, options);
console.log(token);