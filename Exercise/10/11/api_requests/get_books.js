import fetch from "node-fetch";

const url = "http://localhost:3000/book/v1/";

const options = {
    method: "GET",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + process.argv[2]
    } 
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));