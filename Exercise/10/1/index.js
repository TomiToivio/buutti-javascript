import express from "express";
const server = express();
import {fileURLToPath} from "url";
import path from "path";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));
server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(3000, () => {
    // console.log('Listening to port 3000');
});

import studentRouter from "./studentRouter.js";
server.use("/student/", studentRouter);

import {loggerMiddleware, unknownEndpoint} from "./middlewares.js";

server.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/static/index.html"));
});

server.use(unknownEndpoint);