//Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.
//example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm
//1.14 Replace characters (difficult)
//Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
//example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading
// eslint-disable-next-line no-undef
const stringy_string = process.argv[2];

class annoyingSubstring {
    constructor(stringy_string) {
        this.stringy_string = String(stringy_string);
        this.substringAnnoyer();
    }
    substringAnnoyer() {
        this.stringy_string_array = this.stringy_string.split(" ");
        this.stringy_string_array.pop();
        this.stringy_string = this.stringy_string_array.join(" ");
        this.substringConsoleLogger();
    }
    substringConsoleLogger() {
        console.log(this.stringy_string);
    }
}

// eslint-disable-next-line no-unused-vars
const annoying_substring = new annoyingSubstring(stringy_string);