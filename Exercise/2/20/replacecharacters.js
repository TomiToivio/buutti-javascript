/* eslint-disable no-undef */
//Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.
//example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm
//1.14 Replace characters (difficult)
//Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
//example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading
// eslint-disable-next-line no-undef
const replace_character = process.argv[2];
const new_character = process.argv[3];
const stringy_string = process.argv[4];

class replaceCharacters {
    constructor(replace_character, new_character, stringy_string) {
        this.replace_character = String(replace_character);
        this.new_character = String(new_character);
        this.stringy_string = String(stringy_string);
        this.characterReplacer();
    }
    characterReplacer() {
        this.re = new RegExp(`[${this.replace_character}]`, "gi");
        console.log(this.stringy_string.replace(this.re, this.new_character));
        this.stringy_string = this.stringy_string.replace(this.re, this.new_character);
        this.substringConsoleLogger();
    }
    substringConsoleLogger() {
        console.log(this.stringy_string);
    }
}

// eslint-disable-next-line no-unused-vars
const character_replacer = new replaceCharacters(replace_character, new_character, stringy_string);