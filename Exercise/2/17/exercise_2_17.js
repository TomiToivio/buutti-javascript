/* eslint-disable no-undef */
//Create a ATM program to check your balance. Create variables balance, isActive, checkBalance. Write conditional statement that implements the flowchart below.
const balance = process.argv[2];
const isActive = process.argv[3];
const checkBalance = process.argv[4];

class jsATM {
    constructor(balance,isActive,checkBalance) {
        this.balance = Number(balance);
        if(String(isActive) == "true") {
            this.isActive = true;
        } else {
            this.isActive = false;
        }
        if(String(checkBalance) == "true") {
            this.checkBalance = true;
        } else {
            this.checkBalance = false;
        }
        this.checkBalanceMethod();
    }
    checkBalanceMethod() {
        if(this.checkBalance) {
            this.checkIfActive();
        } else {
            console.log("Have a nice day!");
        }
    }
    checkIfActive() {
        if(this.isActive) {
            this.isAccountPositive();
        } else {
            console.log("Your account is not active!");
        }
    }
    isAccountPositive() {
        if(this.balance > 0) {
            console.log("Your balance is " + this.balance + "€.");
        } else if (this.balance == 0) {
            console.log("Your account has no money.");
        } else {
            console.log("Your balance is negative: " + this.balance + "€.");
        }
    }
}

// eslint-disable-next-line no-unused-vars
const ATM = new jsATM(balance,isActive,checkBalance);
