/* eslint-disable no-undef */
//Create a program that takes in 3 names and outputs only initial letters of those name separated with dot.
//example: node .\initialLetters.js Jack Jake Mike -> j.j.m
//1.11 String length comparison
//Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.
//example: node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe
const name_1 = process.argv[2];
const name_2 = process.argv[3];
const name_3 = process.argv[4];
class nameLengths {
    constructor(name_1,name_2,name_3) {
        this.name_1 = String(name_1);
        this.name_2 = String(name_2);
        this.name_3 = String(name_3);
        this.compareNames();
    }
    compareNames() {
        this.name_arr = [this.name_1,this.name_2,this.name_3];
        this.name_arr = this.name_arr.sort((a, b) => b.length - a.length);
        this.consoleLogNames();
    }
    consoleLogNames() {
        console.log(this.name_1[0].toLowerCase() + "." + this.name_2[0].toLowerCase() + "." + this.name_3[0].toLowerCase());
        console.log(this.name_arr[0] + " " + this.name_arr[1] + " " + this.name_arr[2]);
    }
}

// eslint-disable-next-line no-unused-vars
const name_lengths = new nameLengths(name_1,name_2,name_3);
