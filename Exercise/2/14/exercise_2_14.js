/* eslint-disable no-undef */
// Create a program that takes in two numbers a and b from the command line.
// Print out "a is greater" if a is bigger than b, and vice versa, and "they are equal" if they are equal
// Modify program to take in a third string argument c, and print out "yay, you guessed the password", if a and b are equal AND c is "hello world"
const a = process.argv[2];
const b = process.argv[3];
const c = process.argv[4];
class abcComparer {
    constructor(a,b,c) {
        this.a = Number(a);
        this.b = Number(b);
        this.c = c;
    }
    compare() {
        if ((this.a == this.b) && this.c == "hello world") {
            console.log("yay, you guessed the password");
        }
        else if (this.a > this.b) {
            console.log("a is greater.");
        } else if (this.b > this.a) {
            console.log("b is greater.");
        } else if (this.b == this.a) {
            console.log("they are equal.");
        } else {
            console.log("check your input.");
        }       
    }
}

const abcCompare = new abcComparer(a,b,c);
abcCompare.compare();
