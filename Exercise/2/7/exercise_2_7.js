// Create a program that loops through numbers from 1 to 100 and...
for (let ind = 0; ind <= 100; ind++) {
    // if the number is divisible by both (3 and 5), prints “FizzBuzz”
    if((ind % 3 == 0) && (ind % 5 == 0)) {
        console.log("FizzBuzz");
    }
    // if the number is divisible by 3, prints “Fizz”
    else if(ind % 3 == 0) {
        console.log("Fizz");
    }
    // if the number is divisible by 5, prints “Buzz”
    else if(ind % 5 == 0) {
        console.log("Buzz");
    }
    // if no previous conditions apply, prints just the number
    else {
        console.log(ind);
    }
}