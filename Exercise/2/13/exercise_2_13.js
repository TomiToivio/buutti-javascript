/* eslint-disable no-undef */
// Create a program that takes in one argument from command line, a language code (e.g. "fi", "es", "en"). Console.log "Hello World" for the given language for atleast three languages. It should default to console.log "Hello World".
const language = process.argv[2];

switch (language) {
case "fi":
    console.log("Hei maailma!");
    break;
case "en":
    console.log("Hello World!");
    break;
case "ru":
    console.log("Привет, мир!");
    break;
case "cn":
    console.log("你好世界");
    break;
default:
    console.log("Unknown language!");
}