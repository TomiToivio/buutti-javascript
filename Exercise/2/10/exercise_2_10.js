const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
];

let number_students = 0;
let total_score = 0;
let students_average = 0;
let highest_score = 0;
let best_student = "";
let lowest_score = 100;
let worst_student = "";
students.forEach(studentChecker);
function studentChecker(student) {
    let student_name = student.name;
    let student_score = student.score;
    if(highest_score < student_score) {
        highest_score = student_score;
        best_student = student_name;
    }
    if(lowest_score > student_score) {
        lowest_score = student_score;
        worst_student = student_name;
    }
    total_score = total_score + student_score;
    number_students++;
}
students_average = total_score / number_students;
console.log("And the best student is: " + best_student);
console.log("And the worst student is: " + worst_student);
console.log("Student average is: " + students_average);
students.forEach(studentScorer);
function studentScorer(student) {
    let student_grade = 0;
    if(student.score > 94) {
        student_grade = 5;
    } else if (student.score > 79) {
        student_grade = 4;
    } else if (student.score > 59) {
        student_grade = 3;
    } else if (student.score > 39) {
        student_grade = 2;
    } else if (student.score > 0) {
        student_grade = 1;
    } else {
        student_grade = null;
    }

    console.log(student.name + ": " + student_grade);
}

students.forEach(studentAboveAverager);
function studentAboveAverager(student) {
    if(student.score > students_average) {
        console.log(student.name + " is above average!");
    }
}
