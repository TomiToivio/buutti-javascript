const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
// Print the 3rd and 5th items of the array and the array’s length
console.log(arr[2]);
console.log(arr[4]);
console.log(arr.length);
// Then sort the array in alphabetical order and print the entire array
let arr_sorted = arr.sort();
console.log(arr_sorted);
// Finally, add the item “sipuli” to the array, and print out again
arr_sorted.push("sipuli");
console.log(arr_sorted);
// Remove the first item in the array, and print out again. HINT: shift()
arr_sorted.shift();
console.log(arr_sorted);
// Print out every item in this array using .forEach()
arr_sorted.forEach(arrPrinter);
function arrPrinter(vegetable) {
    console.log(vegetable);
}
// Print out every item that contains the letter ‘r’. HINT: includes()
arr_sorted.forEach(rPrinter);
function rPrinter(vegetable) {
    if (vegetable.includes("r")) {
        console.log(vegetable);
    }
}  
