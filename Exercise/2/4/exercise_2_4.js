/* eslint-disable indent */
//Using for loop for each problem, print out the following number sequences:
//0 100 200 300 400 500 600 700 800 900 1000
for (let ind = 0; ind <= 1000; ind = ind + 100) {
    console.log(ind);
}
//1 2 4 8 16 32 64 128
for (let ind = 1; ind <= 128; ind = ind * 2) {
    console.log(ind);
}
//3 6 9 12 15
for (let ind = 3; ind <= 15; ind = ind + 3) {
    console.log(ind);
}
//9 8 7 6 5 4 3 2 1 0
for (let ind = 9; ind >= 0; ind = ind - 1) {
    console.log(ind);
}
//1 1 1 2 2 2 3 3 3 4 4 4
// eslint-disable-next-line no-self-assign
for (let ind = 1; ind <= 4; ind = ind) {
    for(let index = 0; index <= 2; index++) {
        console.log(ind);
    }
    ind++;
}
//0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
for (let ind = 1; ind <= 2; ind++) {
    for(let index = 0; index <= 4; index++) {
        console.log(index);
    }
}
