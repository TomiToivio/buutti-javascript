//Create a program that takes in a number from commandline that represents month of the year. Use console.log to show how many days there are in the given month number.
// eslint-disable-next-line no-undef
const month = process.argv[2];
class monthDays {
    constructor(month) {
        this.month = month;
        this.checkDays();
    }
    checkDays() {
        if(this.month == 1 || this.month == 3 || this.month == 5 || this.month == 7 || this.month == 8 || this.month == 10 || this.month == 12) {
            console.log("31 päivää!");
        } else if (this.month == 2) {
            console.log("28 päivää!");
        } else if (this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11) {
            console.log("31 päivää!");
        } else {
            console.log("Mikäs kuukausi se sellainen on?");
        }
    }
}

// eslint-disable-next-line no-unused-vars
const month_days = new monthDays(month);