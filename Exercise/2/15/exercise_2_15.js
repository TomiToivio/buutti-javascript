/* eslint-disable no-undef */
//From the command line read in three numbers, number1, number2 and number3. Decide their values freely.
//Find the
//a) largest one
//b) smallest one
//c) if they all are equal, print that out
//console.log() its name and value.
const a = process.argv[2];
const b = process.argv[3];
const c = process.argv[4];
class abcComparer {
    constructor(a,b,c) {
        this.a = Number(a);
        this.b = Number(b);
        this.c = Number(c);
        this.largest = Number.MIN_VALUE;
        this.smallest = Number.MAX_VALUE;
    }
    compare() {
        if ((this.a == this.b) && (this.b == this.c)) {
            console.log("They are all equal!");
        } else {
            this.arr = [this.a,this.b,this.c];
            this.arr.sort(function(a, b){return a-b;});
            this.largest = this.arr[2];
            this.smallest = this.arr[0];
            console.log("The biggest number is: " + this.largest);
            console.log("The smallest number is: " + this.smallest);
        }
    }
}

const abcCompare = new abcComparer(a,b,c);
abcCompare.compare();
