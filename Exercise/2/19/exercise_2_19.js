/* eslint-disable no-undef */
//Create a program that takes in a string, and modifies the every letter of that string to upper case or lower case, depending on the input
//example: node .\modifycase.js lower "Do you LIKE Snowmen?" -> do you like snowmen
//example: node .\modifycase.js upper "Do you LIKE Snowmen?" -> DO YOU LIKE SNOWMEN
//NOTE remember to take in the 2nd parameter with quotation marks
const string_operation = process.argv[2];
const input_string = process.argv[3];

class stringCaser {
    constructor(string_operation,input_string) {
        this.string_operation = String(string_operation);
        this.input_string = String(input_string);
        this.caseStrings();
    }
    caseStrings() {
        if((this.input_string == "") || (this.input_string == "undefined")) {
            console.log("Empty string.");
        }
        else if(this.string_operation == "upper") {
            this.upperCaser();
        } else if (this.string_operation == "lower") {
            this.lowerCaser();
        } else {
            console.log("Unknown operation.");
        }
    }
    upperCaser() {
        console.log(this.input_string.toUpperCase());
    }
    lowerCaser() {
        console.log(this.input_string.toLowerCase());
    }
}

// eslint-disable-next-line no-unused-vars
const string_caser = new stringCaser(string_operation,input_string);
