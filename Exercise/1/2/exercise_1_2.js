// Usage: node exercise_1_2.js 1 2

// Read a from command line
// eslint-disable-next-line no-undef
const a = process.argv[2];
// Read b from command line
// eslint-disable-next-line no-undef
const b = process.argv[3];

// Class for exercise
class Exercise {
    // Constructor takes a and b
    constructor(a, b) {
        this.a = Number(a);
        this.b = Number(b);
    }
    // Methods to calculate the exercise
    calcSum() {
        console.log("Sum: " + Number(this.a + this.b));
    }
    calcDiff() {
        console.log("Difference: " + Number(this.a - this.b));
    }
    calcFrac() {
        console.log("Fraction: " + Number(this.a / this.b));
    }
    calcProd() {
        console.log("Product: " + Number(this.a * this.b));
    }
    calcExp() {
        console.log("Exponent: " + Number(this.a ** this.b));
    }
    calcMod() {
        console.log("Modulo: " + Number(this.a % this.b));
    }

}
  
const exercise = new Exercise(a,b);
exercise.calcSum();
exercise.calcDiff();
exercise.calcFrac();
exercise.calcProd();
exercise.calcExp();
exercise.calcMod();
