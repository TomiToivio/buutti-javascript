//Write a program that takes in a string of arbitrary length and outputs a modified string that… (example `` node .\modifyString.js “super cool morning and hello world” ``
// eslint-disable-next-line no-undef
let input_string = process.argv[2];
console.log(input_string);
//...has no white spaces in the beginning or end
input_string = input_string.trim();
console.log(input_string);
//...has a maximum length of 20
if(input_string.length > 20) {
    input_string = input_string.substring(0,19);
}
console.log(input_string);
//...never starts with a capital letter
input_string = input_string.charAt(0).toLowerCase() + input_string.slice(1);
console.log(input_string);
