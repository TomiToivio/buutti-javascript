// Calculate how many seconds there are in a year. Use variables for days, hours, seconds and seconds in a year. Print out the result with console.log.
const days = 365;
const hours = 24;
const minutes = 60;
const seconds = 60;
let seconds_in_a_year = days * hours * minutes * seconds;
console.log(seconds_in_a_year);
