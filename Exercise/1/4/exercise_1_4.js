//By using a single if conditional, write the following conditions:
//The game hearts can only be played with 4 people (playerCount)
const playerCount = 4;
if(playerCount === 4) {
    console.log("Can be played!");
}
//Mark is happy when he is not stressed or if he has ice cream (isStressed, hasIcecream)
const isStressed = false;
const hasIcecream = true;
if(!isStressed && hasIcecream) {
    console.log("Mark is happy!");
}
//It is a beach day if the sun is shining, it is not raining and the temperature is 20 degrees Celsius or above
const sunShining = true;
const notRaining = true;
const temperature = 20;
if(sunShining && notRaining && (temperature >= 20)) {
    console.log("Beach day!");
}
//Arin is happy if he sees either Suzy or Dan on Tuesday night. However, seeing them both at the same time, or being alone, makes him sad.
const seesSuzy = true;
const seesDan = false;
const isTuesdayNight = true;
if((seesSuzy || seesDan) && isTuesdayNight) {
    console.log("Arin is happy!");
}
