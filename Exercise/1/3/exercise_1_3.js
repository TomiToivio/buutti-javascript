//Write a program which has 2 strings str1 and str2
const str1 = "foo";
const str2 = "bar";
//Create a combined string str_sum by using the + operator or using template string and console.log the result.
const str_sum = "foo" + "bar";
console.log(str_sum);
//Check the length of these strings using the “.length” property, e.g., str1.length or str2.length ( try console.logging these ).
console.log(str1.length);
console.log(str2.length);
//Calculate str_avg, the average of str1.length and str2.length
let str_avg = (str1.length + str2.length) / 2;
console.log(str_avg);
//Create if statements where you check the length of str1, str2 and str_sum and console.log the strings only if their length is smaller than str_avg.
function longerThanAverage(input_str, str_avg) {
    if(input_str.length < str_avg) {
        console.log(input_str + " is shorter than average.");
    }
}
longerThanAverage(str1, str_avg);
longerThanAverage(str2, str_avg);
longerThanAverage(str_sum, str_avg);