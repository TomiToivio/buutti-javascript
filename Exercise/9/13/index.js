//Create validator middleware to POST and PUT endpoints that checks that all the required parameters are present.
//Create a 404 Not Found -errorhandler and use it in the application
//Create a middleware for logging in the application. 
//Make the logs readable. req-object contains a lot of needed data for this. Use Google if you need ideas or suggestions of how to achieve this. Do not use any external packages.
//Add helmet to your application’s dependencies and create a basic setup for security.
//Make your application serve static HTML frontend.

/* Tässä versiossa ei ainakaan näitä tehtäviä ole tehty. Onko commit unohtunut? */
// Onkohan tehtävät väärässä järjestyksessä? Muistaakseni tein näitä väärässä järjestyksessä ja osa asioista oli aikaisemmassa tehtävässä eikä tässä. 
import sqlite3 from "sqlite3";
import express from "express";
const server = express();

import {fileURLToPath} from "url";
import path from "path";

import * as helmet from "helmet";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));
server.use(express.json());
server.use(express.urlencoded({extended: false}));
server.use(helmet.contentSecurityPolicy());
server.use(helmet.crossOriginEmbedderPolicy());
server.use(helmet.crossOriginOpenerPolicy());
server.use(helmet.crossOriginResourcePolicy());
server.use(helmet.dnsPrefetchControl());
server.use(helmet.expectCt());
server.use(helmet.frameguard());
server.use(helmet.hidePoweredBy());
server.use(helmet.hsts());
server.use(helmet.ieNoOpen());
server.use(helmet.noSniff());
server.use(helmet.originAgentCluster());
server.use(helmet.permittedCrossDomainPolicies());
server.use(helmet.referrerPolicy());
server.use(helmet.xssFilter());

server.listen(3000, () => {
});

let db = new sqlite3.Database("./books.db", (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log("Connected to the books database.");
    createTable();
});

function createTable() {
    db.run("CREATE TABLE IF NOT EXISTS books(id INTEGER PRIMARY KEY, name TEXT NOT NULL, author TEXT NOT NULL, read BOOLEAN NOT NULL)");
}

import {loggerMiddleware, parameterMiddleware, unknownEndpoint} from "./middlewares.js";
server.use("/book/", loggerMiddleware);
server.use("/books/", loggerMiddleware);
server.use("/book/:id", parameterMiddleware);

server.get("/books/", (request, response) => {
    let bookList = [];
    let sql = "SELECT * FROM books";
    console.log(sql);
    db.all(sql, [], (err, rows ) => {
        console.log(rows);
        console.log(err);
        rows.forEach((row) => {
            console.log(row);
            bookList.push({ 
                "id": Number(row.id),
                "name": String(row.name),
                "author": String(row.author),
                "read": Boolean(row.read),
            });
        });
        response.json(bookList);
    });
});

server.get("/book/:id", (request, response) => {
    let sql = `SELECT * FROM books WHERE id=${Number(request.params.id)}`;
    console.log(sql);
    let responseBook;
    db.all(sql, [], (err, rows ) => {
        console.log(rows);
        rows.forEach((row) => {
            responseBook = {
                "id": Number(row.id),
                "name": String(row.name),
                "author": String(row.author),
                "read": Boolean(row.read),
            };
            console.log(responseBook);
            response.json(responseBook);
        });
    });
});

server.post("/book/", (request, response) => {
    if (request.body.id && request.body.name && request.body.author && request.body.read) {
        let sql = "INSERT INTO books (id,name,author,read) VALUES (" + Number(request.body.id) + ", \"" + String(request.body.name) + "\", \"" + String(request.body.author) + "\", " + Boolean(request.body.read) + ")";
        console.log(sql);
        db.run(sql);
        response.status(200).send({status:"200"});
    } else {
        response.status(400).send({error:"400"});
    }
});

server.put("/book/:id", (request, response) => {
    let bookId = Number(request.params.id);
    if (request.body.name && request.body.author /* && request.body.read */) {
        let sql = "UPDATE books SET name='" + String(request.body.name) + "', author='" + String(request.body.author) + "', read=" + Boolean(request.body.read) + " WHERE id=" + Number(bookId);
        console.log(sql);
        db.run(sql);
        response.status(200).send({status:"200"});
    } else {
        response.status(400).send({error:"400"});
    }
});

server.delete("/book/:id", (request, response) => {
    let bookId = Number(request.params.id);
    db.run(`DELETE FROM books WHERE id=${bookId}`);
    response.status(200).send({status:"200"});
});

server.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/static/index.html"));
});

server.use(unknownEndpoint);