import fetch from "node-fetch";

const url = "http://localhost:3000/book/4";

const options = {
    method: "DELETE",
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));