import fetch from "node-fetch";

const url = "http://localhost:3000/books/";

const options = {
    method: "GET",
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));