import fs from "fs";
export function loggerMiddleware (request, response, next) {
    let logData = request.originalUrl + " " + request.method + " " + Date.now() + "\n";
    if(Object.keys(request.body).length !== 0) {
        logData = logData + JSON.stringify(request.body);
    }
    console.log(logData);
    fs.appendFileSync("log.txt", logData, "utf8");
    next();
}
export function parameterMiddleware (request, response, next) {
    const bookid = request.params.id;
    console.log(bookid);
    if(Number(bookid)) {
        next();
    } else {
        response.status(400).send({error:"400"});
    }
}
export function unknownEndpoint (_req, res) {
    res.status(404).send({error:"404"});
}
//module.exports = { loggerMiddleware }
