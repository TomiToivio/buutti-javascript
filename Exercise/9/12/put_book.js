import fetch from "node-fetch";

const url = "http://localhost:3000/book/3";

const book = {
    name: "Re-learn jQuery: The Future Without Vanilla JavaScript",
    author: "Tomi Toivio",
    read: false,
};

const options = {
    method: "PUT",
    body: JSON.stringify(book),
    headers: {
        "Content-Type": "application/json"
    }
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));