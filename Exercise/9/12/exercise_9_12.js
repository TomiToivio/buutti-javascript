import sqlite3 from "sqlite3"; 
import express from "express";
const server = express();

server.use(express.json());

server.listen(3000, () => {
});

let db = new sqlite3.Database("./books.db", (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log("Connected to the books database.");
    createTable();
});

function createTable() {
    db.run("CREATE TABLE IF NOT EXISTS books(id INTEGER PRIMARY KEY, name TEXT NOT NULL, author TEXT NOT NULL, read BOOLEAN NOT NULL)");
}

server.get("/books/", async (request, response) => { // /api/v1/books
    let bookList = [];
    let sql = "SELECT * FROM books";
    console.log(sql);
    db.all(sql, [], (err, rows) => {
        console.log(rows);
        console.log(err);
        rows.forEach((row) => {
            console.log(row);
            bookList.push({
                "id": Number(row.id),
                "name": String(row.name),
                "author": String(row.author),
                "read": Boolean(row.read),
            });
        });
        response.json(bookList);
    });
});

server.get("/book/:id", (request, response) => {
    let sql = `SELECT * FROM books WHERE id=${Number(request.params.id)}`;
    console.log(sql);
    let responseBook;
    db.all(sql, [], (err, rows) => {
        console.log(rows);
        rows.forEach((row) => {
            responseBook = {
                "id": Number(row.id),
                "name": String(row.name),
                "author": String(row.author),
                "read": Boolean(row.read),
            };
            console.log(responseBook);
            response.json(responseBook);
        });
    });
});

server.post("/book/", (request, response) => {
    if (request.body.id && request.body.name && request.body.author && request.body.read) {
        let sql = "INSERT INTO books (id,name,author,read) VALUES (" + Number(request.body.id) + ", \"" + String(request.body.name) + "\", \"" + String(request.body.author) + "\", " + Boolean(request.body.read) + ")";
        console.log(sql);
        db.run(sql);
        response.status(200).send({ status: "200" });
    } else {
        response.status(400).send({ error: "400" });
    }
});

server.put("/book/:id", (request, response) => {
    let bookId = Number(request.params.id);
    if (request.body.name && request.body.author) {
        let sql = "UPDATE books SET name='" + String(request.body.name) + "', author='" + String(request.body.author) + "', read=" + Boolean(request.body.read) + " WHERE id=" + Number(bookId);
        console.log(sql);
        db.run(sql);
        response.status(200).send({ status: "200" });
    } else {
        response.status(400).send({ error: "400" });
    }
});

server.delete("/book/:id", (request, response) => {
    let bookId = Number(request.params.id);
    db.run(`DELETE FROM books WHERE id=${bookId}`);
    response.status(200).send({ status: "200" });
});