import fetch from "node-fetch";

const url = "http://localhost:3000/book";

const book = {
    id: 4,
    name: "The Angular Side of React - Use Vue.js with React, Angular and jQuery, forget Vanilla Javascript",
    author: "Tomi Toivio",
    read: true,
};

const options = {
    method: "POST",
    body: JSON.stringify(book),
    headers: {
        "Content-Type": "application/json"
    }
};

fetch(url, options).then(res => res.json()).then(res => console.log(res));