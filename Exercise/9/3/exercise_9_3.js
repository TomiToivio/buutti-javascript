import express from 'express';

const server = express();

let counter = 1;

server.get("/counter", (request, response) => {
    if(request.query.number) {
        counter = request.query.number;
    }
    response.send("Counter " + counter);
    counter++;
});
server.listen(5000);