import express from 'express';
import url from 'url';

const server = express();
import fs from 'fs';

server.listen(3000, () => {
    console.log('Listening to port 3000');
});

let students = [];

server.use('/students/', (request, response, next) => {
    let logData = request.url + " " + request.method + " " + Date.now() + "\n";
    fs.appendFileSync('log.txt', logData, 'utf8');
    response.locals.logging = "Logging: " + logData;
    next();
});

server.get('/students/', (request, response) => {
    response.send(`${response.locals.logging} - ${students}`);
});
