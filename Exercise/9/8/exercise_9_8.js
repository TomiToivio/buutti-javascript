import express from 'express';
const server = express();
import {fileURLToPath} from "url";
import path from 'path';

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));
server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(3000, () => {
    console.log('Listening to port 3000');
});

let students = [];

import {loggerMiddleware, unknownEndpoint} from "./middlewares.js";
server.use('/students/', loggerMiddleware);

server.get('/students/', (request, response) => {
    //response.send(`${response.locals.logging} - ${students}`);
    //response.sendFile(path.join(__dirname + "/static/index.html"));
    let responseBody = "<html><head></head><body><ul>";
    students.forEach((student) => {
        //responseBody = responseBody + "<li>" + student.id + " " + student.name + " " + student.email + "</li>";
        responseBody = responseBody + "<li>" + student.id + "</li>";
    });
    responseBody = responseBody + "</ul></body></html>";
    response.send(responseBody);
});

server.get('/student/:id', (request, response) => {
    const searchStudent = students.find((student) => student.id == request.params.id);
    if(searchStudent) {
        let responseBody = "<html><head></head><body>";
        responseBody = responseBody + "<p>ID: " + searchStudent.id + "</p>";
        responseBody = responseBody + "<p>Name: " + searchStudent.name + "</p>";
        responseBody = responseBody + "<p>Email: " + searchStudent.email + "</p>";
        responseBody = responseBody + "</body></html>";
        response.send(responseBody);    
    } else {
        response.status(404).send({error:'404'});
    }
});

server.post('/student/', (request, response) => {
    if (request.body.id && request.body.name && request.body.email) {
        let newStudent = {
            "id": request.body.id,
            "name": request.body.name,
            "email": request.body.email
        };
        const searchStudent = students.find((student) => student.id == request.body.id);
        if(searchStudent) {
            // Mikä olikaan response status jos opiskelija on jo olemassa?
            response.status(201).send({status:'201'});
        } else {
            students.push(newStudent);
            response.status(200).send({status:'200'});
        }
    } else {
        response.status(400).send({error:'400'});
    }
});


