import express from 'express';

const server = express();

let counter = {};

server.get("/counter/:name", (request, response) => {
    let name = request.params.name;
    let times = 1;
    if(counter[name]) {
        counter[name] = counter[name] + 1;
        times = counter[name];
        response.send(name + " was here " + times + " times.");
    } else {
        counter[name] = 1;
        response.send(name + " was here 1 times.");
    }
});
server.listen(5000);
