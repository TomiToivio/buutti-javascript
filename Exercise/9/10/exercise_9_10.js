import express from "express";
const server = express();
import {fileURLToPath} from "url";
import path from "path";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));
server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.listen(3000, () => {
    // console.log('Listening to port 3000');
});

let students = [];

import {loggerMiddleware, unknownEndpoint} from "./middlewares.js";
server.use("/students/", loggerMiddleware);

server.get("/students/", (_request, response) => {
    let responseBody = "<html><head></head><body><ul>";
    students.forEach((student) => {
        responseBody = responseBody + "<li>" + student.id + "</li>";
    });
    responseBody = responseBody + "</ul></body></html>";
    response.send(responseBody);
});

server.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/static/index.html"));
});

server.get("/student/:id", (request, response) => {
    const searchStudent = students.find((student) => student.id == request.params.id);
    if(searchStudent) {
        let responseStudent = {
            "id": searchStudent.id,
            "name": searchStudent.name,
            "email": searchStudent.email,
        };
        response.json(responseStudent);
    } else {
        response.status(404).send({error:"404"});
    }
});

server.post("/student/", (request, response) => {
    if (request.body.id && request.body.name && request.body.email) {
        let newStudent = {
            "id": request.body.id,
            "name": request.body.name,
            "email": request.body.email
        };
        const searchStudent = students.find((student) => student.id == request.body.id);
        if(searchStudent) {
            // Mikä olikaan response status jos opiskelija on jo olemassa?
            response.status(201).send({status:"201"});
        } else {
            students.push(newStudent);
            response.status(200).send({status:"200"});
        }
    } else {
        response.status(400).send({error:"400"});
    }
});

server.put("/student/:id", (request, response) => {
    if (request.body.name || request.body.email) {
        let studentIndex = students.findIndex((student => student.id == request.params.id));
        console.log(studentIndex);
        if(request.body.name) {
            students[studentIndex].name = request.body.name;
        }
        if(request.body.email) {
            students[studentIndex].email = request.body.email;
        }
        console.log(students);
        response.status(200).send({status:"200"});
    } else {
        response.status(400).send({error:"400"});
    }
});

server.delete("/student/:id", (request, response) => {
    function isDeletable(student) { 
        return Number(student.id) !== Number(request.params.id); 
    } 
    function deleteStudent() { 
        students = students.filter(isDeletable); 
    } 
    deleteStudent();
    response.status(200).send({status:"200"});
});

server.use(unknownEndpoint);