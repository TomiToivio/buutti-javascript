import express from "express";
const server = express();
import {fileURLToPath} from "url";
import path from "path";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));

server.listen(3000, () => {
    // console.log('Listening to port 3000');
});

function parseNumbers(numbers) {
    let numberList = numbers.split(",");
    return numberList;
}
// Tämä ei liene ihan tarpeellinen funktio, varsinkaan kun se ei parsi numeroita, vaan palauttaa arrayn, jossa on stringejä

server.get("/add/", (request, response) => {
    let numbers = request.query.numbers;
    let numberList = parseNumbers(numbers);
    let result = 0;
    numberList.forEach((number) => {
        result = result + Number(number);
    });
    response.send(result.toString());
});

server.get("/subtract/", (request, response) => {
    let numbers = request.query.numbers;
    let numberList = parseNumbers(numbers);
    let result = 0;
    let firstNumber = 0;
    numberList.forEach((number) => {
        if(firstNumber === 0) {
            result = Number(result) + Number(number);
        } else {
            result = Number(result) - Number(number);
        }
        firstNumber++;
    });
    response.send("" + result + "");
});

server.get("/multiply/", (request, response) => {
    let numbers = request.query.numbers;
    let numberList = parseNumbers(numbers);
    let result = 0;
    let firstNumber = 0;
    numberList.forEach((number) => {
        if(firstNumber == 0) {
            result = result + Number(number);
        } else {
            result = result * Number(number);
        }
        firstNumber++;
    });
    response.send("" + result + "");
});

server.get("/divide/", (request, response) => {
    let numbers = request.query.numbers;
    let numberList = parseNumbers(numbers);
    let result = 0;
    let zeroError = "false"; //  miksi tämä on string eikä boolean?
    // Olisi loogisempaa tarkistaa ensin, että parametreista mikään ei ole nolla,
    // ja tehdä laskutoimitus vasta sen jälkeen.
    // Vaihtoehtoisesti voi tarkistaa, tuliko laskutoimituksen tulokseksi Infinity
    let firstNumber = 0;
    numberList.forEach((number) => {
        if(firstNumber === 0) {
            result = result + Number(number);
        } else {
            if(Number(number) === 0) {
                response.status(400).send({error:"400"});
                zeroError = "true";
            }
            result = result / Number(number);
        }
        firstNumber++;
    });
    if(zeroError == "false") {
        response.send("" + result + "");
    }
});

// Kuten huomaat, jokaisessa endpointissa lähdetään samalla tavalla liikenteeseen
// Tätä varten oli vinkki tehtävänannossa

// Kaikissa halutaan listastsa yksi tulos. Tällöin reduce käyttöön forEachin sijaan
