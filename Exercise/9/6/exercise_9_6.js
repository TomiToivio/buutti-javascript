import express from 'express';
const server = express();
import fs from 'fs';
import {fileURLToPath} from "url";
import path from 'path';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
server.use("/", express.static(path.join(__dirname + "/static")));

server.listen(3000, () => {
    console.log('Listening to port 3000');
});

let students = [];

server.use('/students/', (request, response, next) => {
    let logData = request.url + " " + request.method + " " + Date.now() + "\n";
    fs.appendFileSync('log.txt', logData, 'utf8');
    next();
});

server.get('/students/', (request, response) => {
    response.sendFile(path.join(__dirname + "/static/index.html"));
});
