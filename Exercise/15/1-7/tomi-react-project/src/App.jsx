import { useState } from "react";
import react from "react";

function NameList(props) {
    const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];
    const DudeList = namelist.map((name,i) => {
        console.log(name);
        if(i % 2 === 0) {
            return(<b key={name}>{name}</b>);
        } else {
            return(<i key={name}>{name}</i>);
        }
    });

    console.log(DudeList);
    
    return (
        <div><p>Namelist:</p>
            {DudeList}
        </div>
    );
}

function HelloFunction(props) {
    return (
        <h1>{props.text}</h1>
    );
}

function HelloYear(props) {
    let yearNow = new Date().getFullYear();
    return (
        <h3>{yearNow}</h3>
    );
}

function App() {
    const [countOne, setCountOne] = useState(0);
    const [countTwo, setCountTwo] = useState(0);
    const [countThree, setCountThree] = useState(0);
    const [inputString, setInputString] = useState('')
    const [showInput, setShowInput] = useState('')
    const [newInput, setNewInput] = useState('')

    const onInputChange = (event) => {
      event.preventDefault();
      console.log(event);
      setNewInput(event.target.value) 
    }

    const handleInputClick = (event) => {
      let newInputVar = document.getElementById("inputMe").value;
      setShowInput(newInputVar);
      setNewInput(newInputVar)
    }

    function StringInput(props) {
      return (
        <div className='InputStuff'>
            <legend>Input:</legend>
            <input id="inputMe" type="text" onChange={onInputChange} value={newInput} />
            <button onClick={handleInputClick}>Submit</button>
            <p>{showInput}</p>
        </div>
    )
    }

    function ButtonOne(props) {
      return(<button onClick={() => setCountOne(countOne + 1)}>{countOne}</button>)
    }

    function ButtonTwo(props) {
      return(<button onClick={() => setCountTwo(countTwo + 1)}>{countTwo}</button>)
    }

    function ButtonThree(props) {
      return(<button onClick={() => setCountThree(countThree + 1)}>{countThree}</button>)
    }
    
    function AllButtons(props) {
      return(<div>{countOne + countTwo + countThree}</div>)
    }

    return (
        <div className="App">
            <HelloFunction text="Hello React!" />
            <HelloYear />
            <NameList />
            <ButtonOne />
            <ButtonTwo />
            <ButtonThree />
            <AllButtons />
            <StringInput />
        </div>
    );
}

export default App;
