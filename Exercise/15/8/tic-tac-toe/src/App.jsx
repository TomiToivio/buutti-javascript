import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [one, setOne] = useState(" ")
  const [two, setTwo] = useState(" ")
  const [three, setThree] = useState(" ")
  const [four, setFour] = useState(" ")
  const [five, setFive] = useState(" ")
  const [six, setSix] = useState(" ")
  const [seven, setSeven] = useState(" ")
  const [eight, setEight] = useState(" ")
  const [nine, setNine] = useState(" ")
  const [turn, setTurn] = useState("X")
  //const [winner, setWinner] = useState("")
  const [result, setResult] = useState("X will move")
  let winner = "";

  function setWinner(value) {
    winner = value;
  }

  function resetGame() {
    setWinner("");
    setOne(" ");
    setTwo(" ")
    setThree(" ")
    setFour(" ")
    setFive(" ")
    setSix(" ")
    setSeven(" ")
    setEight(" ")
    setNine(" ")
    setTurn("X")
    setResult("X will move")
  }

  useEffect(() => {
    setTimeout(() => {
      checkWinner();
    }, 100);
  }, [one,two,three,four,five,six,seven,eight,nine,turn]);

  const ticTacToe = async (event) => {
    event.preventDefault();
    await checkWinner();
    let square = event.target.id;
    if(winner === "") {
    if(square === "one") {
      if(one === " ") {
        setOne(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "two") {
      if(two === " ") {
        setTwo(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "three") {
      if(three === " ") {
        setThree(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "four") {
      if(four === " ") {
        setFour(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "five") {
      if(five === " ") {
        setFive(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "six") {
      if(six === " ") {
        setSix(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "seven") {
      if(seven === " ") {
        setSeven(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "eight") {
      if(eight === " ") {
        setEight(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
    if(square === "nine") {
      if(nine === " ") {
        setNine(turn);
        await checkWinner();
        await nextTurn();
      } else {
        console.log("Already taken :(");
      }
    }
  }
  await checkWinner();
  }

  useEffect(() => {
    checkWinner();  
  }, [one,two,three,four,five,six,seven,eight,nine]);  

  const checkWinner = async () => {
    let players = ["X","O"];
    players.forEach(player => {
    if(one === player && two === player && three === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(four === player && five === player && six === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(seven === player && eight === player && nine === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(one === player && four === player && seven === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(two === player && five === player && eight === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(three === player && six === player && nine === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(one === player && five === player && nine === player) {
      setWinner(player);
      setResult(player + " won!");
    }
    if(seven === player && five === player && three === player) {
      setWinner(player);
      setResult(player + " won!");
    }
  });
  }
  const nextTurn = async () => {
    if(winner !== "") {
      setResult(winner + " won!");
    }
    if(winner === "") {
      if (turn === "X") {
        setTurn("O");
        setResult("O will move")
      } else {
        setTurn("X");
        setResult("X will move")
      }
    }
  }

  const TicTacToeElement = () => {
    return(
      <><div className="row">
        <button id="one" className="tictactoebutton" onClick={ticTacToe}>{one}</button>
        <button id="two" className="tictactoebutton" onClick={ticTacToe}>{two}</button>
        <button id="three" className="tictactoebutton" onClick={ticTacToe}>{three}</button>
      </div><div className="row">
          <button id="four" className="tictactoebutton" onClick={ticTacToe}>{four}</button>
          <button id="five" className="tictactoebutton" onClick={ticTacToe}>{five}</button>
          <button id="six" className="tictactoebutton" onClick={ticTacToe}>{six}</button>
        </div><div className="row">
          <button id="seven" className="tictactoebutton" onClick={ticTacToe}>{seven}</button>
          <button id="eight" className="tictactoebutton" onClick={ticTacToe}>{eight}</button>
          <button id="nine" className="tictactoebutton" onClick={ticTacToe}>{nine}</button>
        </div></>
    )
  }
  
  return (
    <div className="App">
      <TicTacToeElement></TicTacToeElement>
      <button id="resetButton" onClick={resetGame}>Reset</button>
      <p>{result}</p>
  </div>
  )
}

export default App
