import { useState } from 'react'
import './App.css'

function App() {
  const [cards, setCards] = useState(["","","","","","","","","","","","","","","",""])
  const [firstCard, setFirstCard] = useState("");

  let symbolArray = ["+","+","-","-","*","*","~","~","@","@","!","!","^","^","€","€"];

  function CardDeck() {
    for(let index = 0; index < cards.length; index++) {
      let randomSymbolIndex = Math.floor(Math.random() * symbolArray.length);
      let randomSymbol = symbolArray[randomSymbolIndex];
      symbolArray.splice(randomSymbolIndex, 1);
      let oldCards = cards;
      oldCards[index] = randomSymbol;
      setCards(oldCards);
    }
    let cardArray = [];
    function showCard(event) {
      setFirstCard(event.target.dataset.symbol);
    }
    for(let index = 0; index < cards.length; index++) {
      let cardSymbol = cards[index];
      console.log(cardSymbol);
      cardArray.push(<button id={index} className={cardSymbol} key={index} data-symbol={cardSymbol} onClick={showCard}>{cardSymbol}</button>);
    }
    return(
      <div id="cardsDiv">{cardArray}</div>
    )
  }

  return (
    <div className="App">
      <CardDeck></CardDeck>
    </div>
  )
}

export default App
