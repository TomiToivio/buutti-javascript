import fetch from "node-fetch";

const url = "https://tomi-function-app.azurewebsites.net/?name=moi";

const message = {
    name: "Testing testing",
};

const options = {
    method: "POST",
    body: JSON.stringify(message),
    headers: {
        "Content-Type": "application/json"
    } 
};

fetch(url, options).then(res => console.log(res));