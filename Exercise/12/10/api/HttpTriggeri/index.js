module.exports = async function (context, req) {
    context.log("JavaScript HTTP trigger function processed a request.");

    //const name = (req.query.name || (req.body && req.body.name));
    //context.log(name);
    //const responseMessage = name.toUpperCase();
    //context.log(responseMessage);

    let minimum = Number(req.query.minimum);
    let maximum = Number(req.query.maximum);
    let integer = String(req.query.integer);
    let result = Math.random() * (maximum - minimum) + minimum;
    if(integer === "true") {
        result = parseInt(result);
    }
    const responseMessage = String(result);
    context.log(responseMessage);

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };
};