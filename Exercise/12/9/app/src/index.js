import express  from "express";
import path from "path";
import {fileURLToPath} from "url";

const app = express();
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log('Server listening to port', PORT);
});

const __dirname = path.dirname(fileURLToPath(import.meta.url));
app.use("/", express.static(path.join(__dirname + "/static")));

//Create an Express API that produces random numbers. The user should be able to define three parameters: minimum, maximum, and wether the number should be an integer or not. Include a static page that has instructions on how the API works.

app.get("/random", (req, res) => {
    let minimum = Number(req.query.minimum);
    let maximum = Number(req.query.maximum);
    let integer = String(req.query.integer);
    let result = Math.random() * (maximum - minimum) + minimum;
    if(integer === "true") {
        result = parseInt(result);
    }
    res.send(String(result));
});

app.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/public/index.html"));
});
