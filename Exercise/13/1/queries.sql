SELECT * FROM borrows WHERE borrowed_at < '2020-08-15 00:00:00.00000';

SELECT * FROM user.id, borrows WHERE returned_at IS NOT NULL;

SELECT
   B.name,
   B.release_year,
FROM users AS U
LEFT JOIN borrows AS B ON
U.id = B.user_id WHERE u.id = 1;

SELECT
   B.name,
   B.release_year, L.name AS language
FROM books AS B
LEFT JOIN languages AS L ON
B.language_id = L.id WHERE B.release_year > 1960;

UPDATE borrows SET due_date='2022-12-07 08:09:23.801434' WHERE id=1;

INSERT INTO books (name, release_year, genre_id, language_id) VALUES ('Node 2021', 2021, 1, 1);

DELETE FROM borrows WHERE id = 3;