import { v4 as uuidv4 } from 'uuid';
import executeQuery from './db.js';
import * as queries from './queries.js';

const insertProduct = async (product) => {
    const id = uuidv4();
    const params = [id, ...Object.values(product)];
    const result = await executeQuery(queries.insertProduct, params);
    return result;
}

const updateProduct = async (product) => {
    const params = [product.name, product.price, product.id];
    const result = await executeQuery(queries.updateProduct, params);
    return result;
}

const findAll = async () => {
    const result = await executeQuery(queries.findAll);
    return result;
}

const findOne = async (id) => {
    const result = await executeQuery(queries.findOne, [id]);
    return result;
}

const deleteById = async (id) => {
    const params = [id];
    const result = await executeQuery(queries.deleteById, params);
    return result;
}

export default { insertProduct, findAll, findOne, updateProduct, deleteById }