import server from './server.js';
import { createProductsTable } from './db.js';
createProductsTable();
const PORT = process.env.PORT;
server.listen(PORT, () => {
    //console.log("Listening on " + PORT);
})
