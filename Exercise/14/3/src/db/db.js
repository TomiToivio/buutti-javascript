import pg from "pg";
import queries from "./queries.js";
import dotenv from "dotenv/config";
const isProd = process.env.NODE_ENV === "production";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

//import parse from 'pg-connection-string'
//const config = parse('postgres://someuser:somepassword@somehost:381/somedatabase')

export const pool = new pg.Pool({
    host: PG_HOST,
    port: 5432,
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    } finally {
        client.release();
    }
};

export const createMusicTable = async () => {
    await executeQuery(queries.createArtistTable);
    await executeQuery(queries.createAlbumTable);
    await executeQuery(queries.insertArtistsAndAlbums);
    await executeQuery(queries.updateElvis);
    await executeQuery(queries.updateEveliina);
};

export default executeQuery;

