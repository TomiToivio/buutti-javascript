const createArtistTable = `
    CREATE TABLE IF NOT EXISTS "artists" (
        "id" SERIAL,
        "name" VARCHAR(100) NOT NULL,
        PRIMARY KEY (id)
    );`;

const createAlbumTable = `
CREATE TABLE IF NOT EXISTS "albums" (
        "id" SERIAL,
        "name" VARCHAR(100) NOT NULL,
        "artist" INTEGER REFERENCES artists(id) NOT NULL,
        "release_year" SMALLINT NOT NULL,
        PRIMARY KEY (id)
    );`;    

const insertArtistsAndAlbums = `
    INSERT INTO albums(name,artist,release_year) VALUES ('Taivaan kappaleita','Juice Leskinen',1991);
    INSERT INTO albums(name,artist,release_year) VALUES ('Black and Blue','The Rolling Stones',1976);
    INSERT INTO albums(name,artist,release_year) VALUES ('En Kommentoi','Antti Tuisku',2015);
    INSERT INTO albums(name,artist,release_year) VALUES ('Elvis is Back!','Elvis Presley',1958);
    INSERT INTO albums(name,artist,release_year) VALUES ('III','Eveliina',2020);
    `;

const updateElvis = "UPDATE SET release_year=1960 WHERE name='Elvis is Back!';";
const updateEveliina = "UPDATE SET artist='Evelina' WHERE artist='Eveliina';";


export default { createArtistTable, createAlbumTable, insertArtistsAndAlbums, updateElvis, updateEveliina };