import express from "express";
import { createMusicTable } from "./db/db.js";

createMusicTable();

const server = express();

console.log(process.env);
const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});