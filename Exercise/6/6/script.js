let nextToAdd = 0;
function addNew() {
    const newValue = document.getElementById("newToDo").value;
    const newDiv = document.createElement("div");
    document.getElementById("resultArea").appendChild(newDiv);          
    newDiv.setAttribute("id",nextToAdd);
    newDiv.setAttribute("class","removableItem");
    newDiv.innerHTML = "<p>" + newValue + "</p><button onclick=\"removeOld(" + nextToAdd + ")\">Toggle</button>";
    nextToAdd++;
    const removeButton = document.querySelector('#removeButton');
    removeButton.setAttribute("style", "display:block;");
    document.getElementById("newToDo").value = "";
}
function removeOld(toDoId) {
    console.log("removeOld()");
    const oldToDo = document.getElementById(toDoId);
    if(oldToDo.classList.contains('doneToDo')) {
        oldToDo.classList.remove('doneToDo');
    } else {
        oldToDo.classList.add('doneToDo');
    }
}
function removeAll() {
    const doneToDos = document.querySelectorAll('.doneToDo')
    for (const done of [...doneToDos]) {
        done.remove(); 
    }
}