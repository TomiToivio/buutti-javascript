window.addEventListener('load', (event) =>{
    console.log("Load");
    const top = document.querySelector('#top');
    top.addEventListener("mouseover", function(){ 
        top.style.backgroundColor = 'yellow'; 
        console.log("Top"); 
    });
    top.addEventListener("mouseout", function(){ 
        top.style.backgroundColor = 'blue'; 
        console.log("Top"); 
    });
    const left = document.querySelector('#left');
    left.addEventListener("mouseover", function(){ 
        left.style.backgroundColor = 'yellow'; 
        console.log("Left");
    });
    left.addEventListener("mouseout", function(){ 
        left.style.backgroundColor = 'red'; 
        console.log("Left");
    });
    const right = document.querySelector('#right');
    right.addEventListener("mouseover", function(){ 
        right.style.backgroundColor = 'yellow'; 
        console.log("Right");
    });
    right.addEventListener("mouseout", function(){ 
        right.style.backgroundColor = 'blue'; 
        console.log("Right");
    });
});
  