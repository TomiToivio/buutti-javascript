function analyzeText() {
    const textArea = document.getElementById('textToAnalyze');
    let textToAnalyze = textArea.value;
    textToAnalyze = textToAnalyze.toLowerCase();
    textToAnalyze = textToAnalyze.replace(/\./g, '');
    textToAnalyze = textToAnalyze.replace(/\,/g, '');
    textToAnalyze = textToAnalyze.replace(/\;/g, '');
    textToAnalyze = textToAnalyze.replace(/\:/g, '');
    textToAnalyze = textToAnalyze.replace(/\!/g, '');
    textToAnalyze = textToAnalyze.replace(/\"/g, '');
    textToAnalyze = textToAnalyze.replace(/\'/g, '');
    textToAnalyze = textToAnalyze.replace(/\?/g, '');
    let words = textToAnalyze.split(" ");
    const totalLength = words.length;
    const averageLength = words.reduce((acc, cur) => acc + cur.length, 0) / totalLength
    const wordCount = {}
    words.forEach(word => {
        const count = wordCount[word] || 0
        wordCount[word] = count + 1
    })
    let resultArray = [];
    Object.entries(wordCount).sort((a, b) => b[1] - a[1]).forEach(([word, count]) => {
        resultArray.push([word, count])
    });
    resultArray = resultArray.sort((a, b) => {
        return b - a;
    });
    const countWords = resultArray.length; 
    updateResults(averageLength, countWords, resultArray);
}

function updateResults(averageLength, countWords, resultArray) {
    const resultArea = document.getElementById('analysisResults');
    let countP = document.createElement("p");
    countP.textContent = "Words: " + countWords;
    resultArea.appendChild(countP);
    let averageP = document.createElement("p");
    averageP.textContent = "Average length: " + averageLength;
    resultArea.appendChild(averageP);
    for (var i in resultArray) {
        let wordP = document.createElement("p");
        wordP.textContent = i + " " + resultArray[i];
        resultArea.appendChild(wordP);          
    }
}