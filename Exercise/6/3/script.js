window.addEventListener('load', (event) => {
    console.log("Load");
    let isX = 1;
    const squares = document.querySelectorAll(".tictactoe");    
    squares.forEach(element => {
        element.addEventListener("click", function(){ 
            if(element.textContent === "") {
            if(isX === 0) {
                element.textContent = 'O';
                isX = 1;
            } else if (isX === 1) {
                element.textContent = 'X';
                isX = 0;
            }
        }
        });
    });
});
  