//You have a page with a paragraph as below.
//<p>
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget tortor sagittis, mattis quam at, feugiat risus. Cras vitae tellus rhoncus, semper mi eget, sollicitudin odio. Donec eget mattis magna. Pellentesque faucibus risus eu nisl convallis condimentum. Fusce enim libero, malesuada commodo erat eget, volutpat eleifend ipsum. Curabitur maximus bibendum nisl ac volutpat. Phasellus ultricies eleifend enim non convallis. Sed et orci convallis, porta risus porta, maximus orci. Quisque placerat ante elit, nec commodo lorem congue ut. Mauris magna ex, aliquet nec laoreet sed, vulputate vel mauris. Praesent dictum scelerisque lacus, sed eleifend felis efficitur quis. Maecenas malesuada id tortor vel tempor. Vivamus sodales, sapien a vehicula blandit, odio tortor dapibus tellus, vitae bibendum tellus ex faucibus nunc. Etiam a mi consequat nunc aliquet lobortis.
//<p>
//Write a script that
//Divides each sentence into a separate paragraph.
///Highlights all words longer than 6 letters in yellow color.
//Removes the original paragraph (so that there is no duplicate text)
window.addEventListener('load', (event) => {
    console.log("Load");
    const paragraph = document.querySelector("p").textContent;
    // paragraph viittaa nyt textContent parametrin arvoon
    paragraph.textContent = "";
    // tässä taitaa olla nyt yksi .textContent jossain liikaa?
    // tämä olisi sama kuin document.querySelector("p").textContent.textContent = "";
    const sentences = paragraph.split(".");
    let newSentences = "";
    sentences.forEach(sentence => {
        let newSentence = "";
        const words = sentence.split(" ");
        // Pitäisikö noille pilkuille tehdä jotain?
        // Ei tarvitse. Jos haluaa hifistellä, niin ne voi ottaa pois kun laskee sanan pituutta
        words.forEach(word => {
            if(word.length > 6) {
                newSentence = newSentence + " <span class=\"yellow\">" + word + "</span>";
                // Tässä olisi selkeämpää käyttää hipsuja
                // newSentence + `<span class="yellow">${word}</span>`
            } else {
                newSentence = newSentence + " " + word;
            }
            console.log(newSentence); 
        });
        if(newSentence !== " ") {
            newSentences = newSentences + newSentence + ".";       
        } 
        console.log(newSentences);
        document.getElementById("loremipsum").innerHTML = newSentences;
    });
});

/*
Ohjelma ei jaa alkuperäisen kappaleen tekstiä lauseittain uusiksi kappaleiksi. Lopputuloksessa tulisi olla
<p> elementti jokaista lausetta kohti.
*/
  