//A sliding tile puzzle is the kind of puzzle where you have a grid of pieces, with one piece missing. You can move any piece up, down, or sideways to the place of the missing piece. The goal is to arrange the tiles in order.
//An example of a sliding tile puzzle can be found here
//Crete a 4x4 sliding tile puzzle. The game should have 16 squares with numbers 1-15 in them, plus the empty square. Every time the player clicks a square, the game should check if the piece is one of the pieces that can be moved. If so, the game swaps the clicked square and the empty square. If not, nothing happens.
//After each click the game should check if the tiles are in order. If so, the game ends and the empty slot is replaced by number 16. A message telling that the player has finished the game appears to the screen.
//Extra: Replace the numbers in the game with a grid of pictures. You can use an external service, like postcron.com to create a grid of images. When the game finishes, the missing tile should be replaced with the remaining image piece.
window.addEventListener('load', (event) => {
    randomizeNumbers();
});
// ^ Jos kutsut vain yhtä funktiota ilman parametreja, et tarvitse anonyymiä nuolifunktiota 
// window.onload = randomizeNumbers;

function arrayComparer(numberArray,numberAdded) {
    // nimeämisestä: numberAdded kuulostaisi olevan numero, mutta sitten näyttää siltä, että se on array
    // jää epäselväksi miksi meillä on array, jonka nimi on numberAdded
    // myös, mikä numberArray? mihin numeroita käytetään, miksi ne ovat tässä funktiossa,
    // ja miksi meillä on funktio, joka vertailee kahta arraytä?
    let numberRandomizer = Math.floor(Math.random() * numberArray.length);
    let numberToAdd = numberArray[numberRandomizer];
    while(numberAdded.includes(numberToAdd)) {
        numberRandomizer = Math.floor(Math.random() * numberArray.length);
        numberToAdd = numberArray[numberRandomizer];    
    } 
    return numberToAdd;
}
function randomizeNumbers() {
    let numberArray = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","0"];
    let numberAdded = [];
    for(let x = 1; x < 5; x++) {
        for(let y = 1; y < 5; y++) {
            let numberToAdd = arrayComparer(numberArray,numberAdded);
            numberAdded.push(numberToAdd);
            // numberArray.splice(numberRandomizer);
            console.log(numberArray);
            let elementToAdd = String(x) + String(y);
            if(Number(numberToAdd) === 0) {
                numberToAdd = "";
            }
            console.log("New number for " + elementToAdd + " is " + numberToAdd);
            let addDiv = document.getElementById(elementToAdd);
            addDiv.textContent = numberToAdd;
        }
    }
}

// Olisi selkeämpää, jos meillä olisi erilliset funktiot, jotka sotkevat arrayn,
// ja toinen, joka sitten käyttäisi satunnaisjärjestettyä arraytä, eli muokkaisi DOMia.

function checkVictory() {
    let correctNumber = 1;
    for(let x = 1; x < 5; x++) {
        for(let y = 1; y < 5; y++) {
            if(x === 4 && y === 4) {
                console.log("You won!");
                alert("You won!");            
                return true;
            }
            let elementToCheck = String(x) + String(y);
            console.log("Checking win for " + elementToCheck);
            let div = document.getElementById(elementToCheck);
            let value = div.textContent;
            console.log("Value at " + elementToCheck + " is " + value);
            console.log("Correct number is " + correctNumber);
            if(Number(value) === Number(correctNumber)) {
                console.log(correctNumber + " is correct");
                correctNumber++;
            } else {
                console.log("Didn't win");
                return false;
            }
        }
    }
    console.log("You won!");
    alert("You won!");
    return true;
}
function doMove(gridID, canMove) {
    console.log("Move " + gridID + " to " + canMove);
    let oldDiv = document.getElementById(gridID);
    let oldValue = oldDiv.textContent;
    console.log(oldValue);
    let newDiv = document.getElementById(canMove);
    newDiv.textContent = oldValue;
    oldDiv.textContent = "";
    checkVictory();
}
function gridClick(gridID) {
    console.log(gridID);
    let gridDiv = document.getElementById(gridID);
    let gridValue = gridDiv.textContent;
    console.log(gridValue);
    let gridCoordinates = String(gridID);
    let X = gridCoordinates[0];
    let Y = gridCoordinates[1];
    console.log(X);
    console.log(Y);
    if(gridValue !== "") {
        let canMove = checkNeighbors(X,Y);
        console.log("canMove: " + canMove);
        if(canMove) {
            doMove(gridID, canMove);
        }
    } else {
        console.log("Empty!");
    }
}
function checkNeighbors(X,Y) {
    let idToCheck = "";
    console.log("Check neighbors for " + X + Y);
    if((Number(X) - 1) === 0) {
        console.log("Out of board above");
    } else {
        idToCheck = String(String(X-1) + String(Y));
        console.log("Check: " + idToCheck);
        let elementToCheck = document.getElementById(idToCheck);
        let elementValue = elementToCheck.textContent;
        console.log(elementValue);
        if(elementValue === "") {
            console.log("Can move!");
            return(idToCheck);
        } else {
            console.log("Cannot move!");
        }
    }
    if((Number(X) + 1) === 5) {
        console.log("Out of board below")
    } else {
        idToCheck = String(String(Number(Number(X)+1)) + String(Y));
        console.log("Check: " + idToCheck);
        let elementToCheck = document.getElementById(idToCheck);
        let elementValue = elementToCheck.textContent;
        console.log(elementValue);
        if(elementValue === "") {
            console.log("Can move!");
            return(idToCheck);
        } else {
            console.log("Cannot move!");
        }
    }
    if((Number(Y) - 1) === 0) {
        console.log("Out of board left");
    } else {
        idToCheck = String(String(X) + String(Y - 1));
        console.log("Check: " + idToCheck);
        let elementToCheck = document.getElementById(idToCheck);
        let elementValue = elementToCheck.textContent;
        console.log(elementValue);
        if(elementValue === "") {
            console.log("Can move!");
            return(idToCheck);
        } else {
            console.log("Cannot move!");
        }
    }
    if((Number(Y) + 1) === 5) {
        console.log("Out of board right");
    } else {
        idToCheck = String(String(X) + String(Number(Number(Y)+1)));
        console.log("Check: " + idToCheck);
        let elementToCheck = document.getElementById(idToCheck);
        let elementValue = elementToCheck.textContent;
        console.log(elementValue);
        if(elementValue === "") {
            console.log("Can move!");
            return(idToCheck);
        } else {
            console.log("Cannot move!");
        }
    }
    return false;
}

// Peli toimii ja bugeja ei löydy! Hyvin tehty!
// Koodista saa kyllä selvää, mutta suurimpana kritiikkinä esittäisin
// selkeyden puutteen. Yritä saada koodi dokumentoimaan itsensä.
// Erityisesti tällaisessa tapauksessa, jossa olet mallintanut laudan
// omalla tavallasi (tietysti, koska tehtävänannossa ei mitään mallia annettu)
// on tärkeää, että mallinnuksen ajatus tulee jotenkin, joko koodin tai 
// kommenttien, kautta esiin koodin lukijalle.

// Runsas console.login käyttö debugin tavoinsaa myöskin aikaan vaikutelman,
// että kyseessä on keskeneräinen projekti.