# Docker-kuvien koot
node:latest: 994.73 MB
node:slim: 244.32 MB
node:alpine: 168.46 MB
alpine:latest: 64.76 MB

Kuvien koot menevät oikeastaan päinvastoin kuin olisi olettanut. Debian paisuttaa node:latest-kuvaa. Tämän perusteella kannattaisi käyttää aina alpine:latestia?
