import express from "express";
const server = express();
import "dotenv/config";
const PASSWORD = process.env.PASSWORD;
const SECRETUSER = process.env.SECRETUSER;

server.listen(3000, () => {
    console.log("Listening to port 3000");
});

let notes = [{
    "id": 1,
    "title": "I am an example note",
    "content": "Note goes here"
}];

let secretNotes = [{
    "id": 1,
    "title": "I am an example secretnote",
    "content": "Secret note goes here"
}];

server.get("/:id", (request, response) => {
    const searchNote = notes.find((note) => note.id == request.params.id);
    if(searchNote) {
        let responseNote = {
            "id": searchNote.id,
            "title": searchNote.title,
            "content": searchNote.content,
        };
        response.json(responseNote);
    } else {
        response.status(404).send({error:"404"});
    }
});

server.post("/", (request, response) => {
    if (request.body.id && request.body.title && request.body.content) {
        let newNote = {
            "id": request.body.id,
            "title": request.body.title,
            "content": request.body.content
        };
        const searchNote = notes.find((note) => note.id == request.body.id);
        if(searchNote) {
            response.status(401).send({status:"401"});
        } else {
            notes.push(newNote);
            response.status(200).send({status:"200"});
        }
    } else {
        response.status(400).send({error:"400"});
    }
});

server.put("/:id", (request, response) => {
    if (request.body.title || request.body.content) {
        let noteIndex = notes.findIndex((note => note.id == request.params.id));
        if(request.body.title) {
            notes[noteIndex].title = request.body.title;
        }
        if(request.body.content) {
            notes[noteIndex].content = request.body.content;
        }
        response.status(200).send({status:"200"});
    } else {
        response.status(400).send({error:"400"});
    }
});

server.delete("/:id", (request, response) => {
    function isDeletable(note) { 
        return Number(note.id) !== Number(request.params.id); 
    } 
    function deleteNote() { 
        notes = notes.filter(isDeletable); 
    } 
    deleteNote();
    response.status(200).send({status:"200"});
});

server.get("/secret/:id", (request, response) => {
    if(SECRETUSER === request.body.user && PASSWORD === request.body.password) {
        const searchNote = secretNotes.find((note) => note.id == request.params.id);
        if(searchNote) {
            let responseNote = {
                "id": searchNote.id,
                "title": searchNote.title,
                "content": searchNote.content,
            };
            response.json(responseNote);
        } else {
            response.status(404).send({error:"404"});
        }
    } else {
        response.status(404).send({error:"404"});
    }
});

server.post("/secret/", (request, response) => {
    if(SECRETUSER === request.body.user && PASSWORD === request.body.password) {
        if (request.body.id && request.body.title && request.body.content) {
            let newNote = {
                "id": request.body.id,
                "title": request.body.title,
                "content": request.body.content
            };
            const searchNote = secretNotes.find((note) => note.id == request.body.id);
            if(searchNote) {
                response.status(401).send({status:"401"});
            } else {
                secretNotes.push(newNote);
                response.status(200).send({status:"200"});
            }
        } else {
            response.status(400).send({error:"400"});
        }
    } else {
        response.status(404).send({error:"404"});
    }
});

server.put("/secret/:id", (request, response) => {
    if(SECRETUSER === request.body.user && PASSWORD === request.body.password) {
        if (request.body.title || request.body.content) {
            let noteIndex = secretNotes.findIndex((note => note.id == request.params.id));
            if(request.body.title) {
                secretNotes[noteIndex].title = request.body.title;
            }
            if(request.body.content) {
                secretNotes[noteIndex].content = request.body.content;
            }
            response.status(200).send({status:"200"});
        } else {
            response.status(400).send({error:"400"});
        }
    } else {
        response.status(404).send({error:"404"});
    }
});

server.delete("/secret/:id", (request, response) => {
    function isDeletable(secretNote) { 
        return Number(secretNote.id) !== Number(request.params.id); 
    } 
    function deleteNote() { 
        secretNotes = secretNotes.filter(isDeletable); 
    }
    if(SECRETUSER === request.body.user && PASSWORD === request.body.password) {
        deleteNote();
        response.status(200).send({status:"200"});
    } else {
        response.status(404).send({error:"404"});
    }
});