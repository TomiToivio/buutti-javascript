let firstTerm = 0;
let secondTerm = 1;
let nextTerm = 0;

function funkyFibonacci() {
    nextTerm = firstTerm + secondTerm;
    console.log(nextTerm);
    firstTerm = secondTerm;
    secondTerm = nextTerm;
    setTimeout(() => {
        funkyFibonacci();
    }, "1000");
}
  
funkyFibonacci();