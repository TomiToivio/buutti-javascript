import fetch from "node-fetch";
import express from "express";
const server = express();

server.listen(3001, () => {
    console.log("Listening to port 3001");
});

const url = "http://server:3000/student/1";

const options = {
    method: "GET",
};
server.get("/", (request, response) => {
    fetch(url, options).then(res => res.json()).then(res => {
        console.log(res);
        response.send(res);
    });
});

