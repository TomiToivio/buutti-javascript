import fs from 'fs';
export function loggerMiddleware (request, response, next) {
    let logData = request.originalUrl + " " + request.method + " " + Date.now() + "\n";
    if(Object.keys(request.body).length !== 0) {
        logData = logData + JSON.stringify(request.body);
    }
    console.log(logData);
    fs.appendFileSync('log.txt', logData, 'utf8');
    next();
}
export function unknownEndpoint (_req, res) {
    res.status(404).send({error:'404'});
}
//module.exports = { loggerMiddleware }
