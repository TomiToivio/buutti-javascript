const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
function arrayRandomizer(array) {
    let randomArray = [];
    const arrayLength = array.length;
    let i = 0;
    while(i < arrayLength) {
        let randomIndex = Math.floor(Math.random() * array.length);
        randomArray = randomArray.concat(array.splice(randomIndex,1))
        i++;
    }
    return randomArray;
}
console.log(arrayRandomizer(array));