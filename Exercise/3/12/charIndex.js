const inputString = process.argv[2];
const characters = "abcdefghijklmnopqrstuvxyz";
function wordToCharIndex(inputString) {
    let splitString = inputString.split("");
    let charIndexString = "";
    splitString.forEach(function(character){
        let characterIndex = characters.indexOf(character) + 1;
        charIndexString = charIndexString + characterIndex;
    });
    return charIndexString;
}
console.log(wordToCharIndex(inputString));
