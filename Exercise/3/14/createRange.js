/* eslint-disable no-undef */
const numberOne = Number(process.argv[2]);
const numberTwo = Number(process.argv[3]);
function createRange(numberOne, numberTwo) {
    let range = [];
    if(numberOne < numberTwo) {
        for(let i = numberOne; i <= numberTwo; i++) {
            range.push(i);
        }    
    } else {
        for(let i = numberOne; i >= numberTwo; i--) {
            range.push(i);
        } 
    }
    return range;
}
console.log(createRange(numberOne,numberTwo));