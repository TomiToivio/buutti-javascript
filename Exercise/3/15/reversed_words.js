const sentence = String(process.argv[2]);
function reversedWords(sentence) {
    let words = sentence.split(" ");
    let reversedSentence = "";
    words.forEach(function(word) {
        let reversedWord = word.split("").reverse().join("");
        reversedSentence = reversedSentence + " " + reversedWord; 
    });
    return reversedSentence;
}
console.log(reversedWords(sentence));
