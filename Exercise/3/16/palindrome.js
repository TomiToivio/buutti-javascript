const word = String(process.argv[2]);
function checkPalindrome(word) {
    let reversedWord = word.split("").reverse().join("");
    if(reversedWord == word) {
        return "Yes, \"" + word + "\" is a palindrome";
    } else {
        return "No, \"" + word + "\" is not a palindrome";
    }
}
console.log(checkPalindrome(word));