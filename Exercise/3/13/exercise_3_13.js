const competitors = ['Julia', 'Mark', 'Spencer', 'Ann' , 'John', 'Joe'];
const ordinals = ['st', 'nd', 'rd', 'th'];
function competitorPlacements(competitors, ordinals) {
    let competitorArray = [];
    competitors.forEach(function(competitor, index) {
        let competitorString = "";
        competitorString = index + 1;
        const ordinal = index < 3 ? ordinals[index] : ordinals[3]
        competitorString = competitorString + ordinal + " competitor was " + competitor;
        competitorArray.push(competitorString);
    });
    return competitorArray;
}
console.log(competitorPlacements(competitors,ordinals));