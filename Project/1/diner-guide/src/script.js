function fetchData() {
    let url = "https://jsc-myhelsinki-api.azurewebsites.net/v1/places/?tags_filter=Restaurant";
    fetch(url).then(response => response.json()).then(data => {
        //console.log(data);
        let restaurantData = data["data"];
        //console.log(restaurants);
        //restaurants.forEach((restaurant) => 
        //{
            //console.log(place);
        //    console.log(restaurant["name"]["fi"]);
        //}
        //);
        //console.log(restaurantData);
        return restaurantData;
    });
}
const restaurants = fetchData();

const render = (data) => {
    document.getElementById('restaurant-list').replaceChildren()
    data.map(restaurant => {
        const div = document.createElement('div')
        div.innerHTML = restaurant["name"]["fi"]
        div.classList.add('clickable')
        div.onclick = () => {
            const div = document.createElement('div')
            div.id = 'restaurant-info'
            const h = document.createElement('h2')
            h.innerText = restaurant["name"]["fi"]
            const i = document.createElement('img')
            i.src = './img/google-maps-logo.png'
            i.width = 20
            const a = document.createElement('a')
            a.href = 'https://www.google.fi/maps/place/' + restaurant.address["address"]["street_address"].trim().replace(/\s/g, '+')
            const div2 = document.createElement('div')
            div2.innerText = restaurant.address["address"]["street_address"]
            const div3 = document.createElement('div')
            const p = document.createElement('p')
            p.innerText = restaurant["description"]["body"]
            a.appendChild(i)
            div3.replaceChildren(a, div2)
            div.replaceChildren(h, div3, p)
            document.getElementById('restaurant-info').replaceWith(div)
        }
        document.getElementById('restaurant-list').appendChild(div)
    })
}

document.getElementById('search-input').oninput = () => render(restaurants.filter(r => r["name"]["fi"].startsWith(document.getElementById('search-input').value)))
